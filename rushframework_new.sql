-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2017 at 06:18 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rushframework_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grouping` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `name`, `grouping`, `enable`, `title`) VALUES
(29, 'PUBLIC', 'PUBLIC', 1, 'Rush Framework'),
(30, 'ADD FEATURE', 'VITAL', 1, 'Feature Management'),
(31, 'EDIT FEATURE', 'VITAL', 1, 'Feature Management'),
(32, 'VIEW FEATURE', 'VITAL', 1, 'Feature Management'),
(33, 'FEATURE DASHBOARD', 'VITAL', 1, 'Feature Management'),
(34, 'ERROR PAGE', 'PUBLIC', 1, 'Error Occured'),
(35, 'SIGN UP', 'PUBLIC', 1, 'Sign Up'),
(36, 'ACTIVATION', 'PUBLIC', 1, 'Activate My Account'),
(37, 'FORGOT PASSWORD', 'PUBLIC', 1, 'Fogot Password'),
(38, 'RESEND ACTIVATION', 'PUBLIC', 1, 'Resend Me My Activation Link'),
(39, 'LOGIN', 'PUBLIC', 1, 'LogIn'),
(40, 'DASHBOARD', 'BACK OFFICE', 1, 'My Back Office'),
(41, 'DASHBOARD CORE', 'CORE MANAGEMENT', 1, 'Core Management Dashboard'),
(42, 'DASHBOARD GALLERY', 'GALLERY', 1, 'My Gallery'),
(43, 'ADD IMAGE VIDEO', 'GALLERY', 1, 'Add Image or Video'),
(44, 'DOWNLOAD GALLERY', 'PUBLIC', 1, 'Download From My Gallery'),
(45, 'DASHBOARD FILES', 'FILES', 1, 'My Files'),
(46, 'DOWNLOAD FILES', 'PUBLIC', 1, 'Download My File'),
(47, 'ADD FILE', 'FILES', 1, 'Add A File In My Files'),
(48, 'DASHBOARD ROLE', 'ROLE', 1, 'Role Dashboard'),
(49, 'ADD ROLE', 'ROLE', 1, 'Add A Role'),
(50, 'VIEW ROLE', 'ROLE', 1, 'View Role'),
(51, 'CHANGE USER ROLE', 'ROLE', 1, 'Change User\'s Role'),
(52, 'ACTIVATE FEATURE FOR ROLE', 'ROLE', 1, 'Activate the feature for a particular role'),
(53, 'DESACTIVATE FEATURE FOR ROLE', 'ROLE', 1, 'Desactivate a feature for a role'),
(54, 'DASHBOARD USER MANAGEMENT', 'USER MANAGEMENT', 1, 'Users Dashboard'),
(55, 'VIEW USER', 'USER MANAGEMENT', 1, 'View USER'),
(56, 'PROFILE PICTURE', 'USER', 1, 'Change Profile Picture'),
(57, 'CREATE USER', 'USER MANAGEMENT', 1, NULL),
(58, 'CHANGE USER PROFILE PICTURE', 'USER MANAGEMENT', 1, 'Change Another User\'s Profile Picture');

-- --------------------------------------------------------

--
-- Table structure for table `featureaccess`
--

CREATE TABLE `featureaccess` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `f29` tinyint(4) NOT NULL DEFAULT '1',
  `f30` tinyint(4) NOT NULL DEFAULT '0',
  `f31` tinyint(4) NOT NULL DEFAULT '0',
  `f32` tinyint(4) NOT NULL DEFAULT '0',
  `f33` tinyint(4) NOT NULL DEFAULT '0',
  `f34` tinyint(4) NOT NULL DEFAULT '1',
  `f35` tinyint(4) NOT NULL DEFAULT '1',
  `f36` tinyint(4) NOT NULL DEFAULT '1',
  `f37` tinyint(4) NOT NULL DEFAULT '1',
  `f38` tinyint(4) NOT NULL DEFAULT '1',
  `f39` tinyint(4) NOT NULL DEFAULT '1',
  `f40` tinyint(4) NOT NULL DEFAULT '0',
  `f41` tinyint(4) NOT NULL DEFAULT '0',
  `f42` tinyint(4) NOT NULL DEFAULT '0',
  `f43` tinyint(4) NOT NULL DEFAULT '0',
  `f44` tinyint(4) NOT NULL DEFAULT '1',
  `f45` tinyint(4) NOT NULL DEFAULT '0',
  `f46` tinyint(4) NOT NULL DEFAULT '1',
  `f47` tinyint(4) NOT NULL DEFAULT '0',
  `f48` tinyint(4) NOT NULL DEFAULT '0',
  `f49` tinyint(4) NOT NULL DEFAULT '0',
  `f50` tinyint(4) NOT NULL DEFAULT '0',
  `f51` tinyint(4) NOT NULL DEFAULT '0',
  `f52` tinyint(4) NOT NULL DEFAULT '0',
  `f53` tinyint(4) NOT NULL DEFAULT '0',
  `f54` tinyint(4) NOT NULL DEFAULT '0',
  `f55` tinyint(4) NOT NULL DEFAULT '0',
  `f56` tinyint(4) NOT NULL DEFAULT '0',
  `f57` tinyint(4) NOT NULL DEFAULT '0',
  `f58` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `featureaccess`
--

INSERT INTO `featureaccess` (`id`, `role_id`, `f29`, `f30`, `f31`, `f32`, `f33`, `f34`, `f35`, `f36`, `f37`, `f38`, `f39`, `f40`, `f41`, `f42`, `f43`, `f44`, `f45`, `f46`, `f47`, `f48`, `f49`, `f50`, `f51`, `f52`, `f53`, `f54`, `f55`, `f56`, `f57`, `f58`) VALUES
(6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(5, 5, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0),
(7, 7, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_set` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `profile_set`, `created_at`, `updated_at`) VALUES
(5, 'DEFAULT', 1, '2016-12-27 18:39:09', '2016-12-27 18:39:09'),
(6, 'ADMIN', 1, '2016-12-27 18:39:10', '2016-12-27 18:39:10'),
(7, 'CONTENT_MANAGER', 1, '2016-12-30 11:36:22', '2016-12-30 11:36:22');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sugar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `online` tinyint(1) NOT NULL,
  `login_attempt` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `activation_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role_id`, `email`, `password`, `sugar`, `image`, `created_at`, `updated_at`, `last_login`, `online`, `login_attempt`, `active`, `activation_hash`, `name`, `surname`, `data`) VALUES
(9, 6, 'ajir.edun@gmail.com', 'e7165352c5b48b2f049868a982a85ac1ebb87faa733d9a04ebb40f422b135fb0', 'ebb87faa733d9a04ebb40f422b135fb0', '/data/users/9/profile_picture.jpg', '2016-12-27 18:39:13', '2016-12-27 18:39:13', '2017-01-10 23:34:25', 1, 0, 1, '80177fe965b78111f4dfee80cfdc3689', 'Ajir', 'Edun', NULL),
(10, 7, 'rushdana.janoo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e7f3fcfed9109f27a4b9e4abd169d6e43', '7f3fcfed9109f27a4b9e4abd169d6e43', '/data/users/10/profile_picture.png', '2017-01-07 15:45:58', '2017-01-07 15:45:58', NULL, 0, 0, 0, 'c22abfa379f38b5b0411bc11fa9bf92f', 'Rushdana', 'Janoo', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `featureaccess`
--
ALTER TABLE `featureaccess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD KEY `IDX_8D93D649D60322AC` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `featureaccess`
--
ALTER TABLE `featureaccess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
