$(document).ready(function(){

    //ajaxify
    $('body').on('click','.rf-ajaxify',function(){
        var onclose = $(this).attr('data-onclose');
        generateDialog(onclose);
        $.get($(this).attr('data-link'),null,function(data,status,xhr){
            $(".rf-ajaxify-container").html(data);
        });
    });

    $('body').on('submit', '.rf-ajaxify-form', function (e) {
        e.preventDefault();
        $(".rf-ajaxify-container").html('<div class="rush_framework_loader"></div>');
    });


    /**
     * PAGINATION
     */
    $('body').on('change','.rf-pagination-select',function(){
        var url = window.location.href;
        var val = $(this).val();
        var param = val.split("=")[0];
        var paramValue = val.split("=")[1];
        var querystring = window.location.search;
        window.location.href= addOrUpdateParameterInUrl(querystring,param,paramValue);

    });

    $('body').on('click','.rf-pagination-link',function(event){
        event.preventDefault();
        var val = $(this).attr('href');
        var param = val.split("=")[0];
        var paramValue = val.split("=")[1];
        var querystring = window.location.search;
        window.location.href= addOrUpdateParameterInUrl(querystring,param,paramValue);
    });


    //list buttons
    $('body').on('click','.rf-list-buttons-initiator',function(event){
        $('.rf-list-buttons-wrapper').toggle();
    });

    $('body').on('click','.rf-file-download',function(event){
        //event.preventDefault();
        btnSubmitDownload($(this));
    });

    $('body').on('click','.rf-note-commands-ajaxifier',function(event){
        //event.preventDefault();
        var url = $(this).attr('rf-href');
        var container = $(this).attr('rf-container');
        $.ajax({
            url: url,
            success: function(result){
                $(container).html(result);
            }
        });
    });

    $('body').on('change','#form_manageFile',function(event){
        $(".ajs-input").val($('#form_manageFile').val());
    });

    $('body').on('change','#form_manageGallery',function(event){
        $(".ajs-input").val($('#form_manageGallery').val());
    });

    $('.video').height($('.video').width()*0.66);
});


/**
 * download button
 * @param element
 */
function btnSubmitDownload(element) {
    var value = element.attr('data-rf');
    $('#download_input').val(value);
    $('#form_download_file').submit();
}

/**
 * Load zones using ajax after page load
 * @param el
 * @param url
 * @param container
 */
function ajaxLoadBlock(el, url, container)
{
    var id = el.attr('class');
    if (container == null) {
        el.html(
            "<section class='rush_framework_ajax_container "+ id +" '><div class='rush_framework_loader'></div><br/>loading...</section>"
        );
    } else {
        container.html(
            "<section class='rush_framework_ajax_container "+ id +" '><div class='rush_framework_loader'></div><br/>loading...</section>"
        );
    }


    if (url == null) {
        url = el.attr('data-url');
    }

    if (url != "" || url != null) {
        $.ajax({
            url: url,
            success: function(result){
                if (container == null) {
                    el.html(result);
                } else {
                    container.html(result);
                }
                setSameHeights();
                history.pushState(result, null, url);
            },
            error: function(result){
                alertify.error("Error Occured while fetching : "+url);
            }
        });
    }
}

//menu
function menuContentLoad(event, id, ajaxify){
    event.stopPropagation();
    var el = $("#"+id);
    if (ajaxify) {
        $('.rf-backoffice-menu').removeClass('active');
        el.addClass('active');
        ajaxLoadBlock(el, null, $(".rush_framework_backoffice_content"));
    } else {
        var url = el.attr('data-url');
        window.location.href = url;
    }
};

function ajaxifyContent(el) {
    ajaxLoadBlock($(el), null, $(".rush_framework_backoffice_content"));
}

//background

function rushBackground(id, url) {
    var el = $(id);
    $.ajax({
        url: url,
        success: function(result){
            if (result == "false") {
                alertify.error("Error Occured while loading background");
            } else {
                var len = result.images.length;

                interation = (Math.floor(Math.random() * len) + 1) -1;
                var img = 'url('+result.images[interation].url+')';
                el.css("background-image", img);

                setInterval(function(){
                    interation = (Math.floor(Math.random() * len) + 1) -1;
                    var img = 'url('+result.images[interation].url+')';
                    el.css("background-image", img);
                }, 10000);
            }
        },
        error: function(result){
            alertify.error("Error Occured while fetching background : "+url);
        }
    });
}

//note
function getNotesForUser(id, url) {
    var el = $(id);
    setInterval(function(){
        $.ajax({
            url: url,
            success: function(result){
                el.html(result);
            }
        });
    },10000);

}


//fullscreen
function toggleFullScreen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
        (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}


// Pagination
function addOrUpdateParameterInUrl(querystring,param,paramValue){
    if (querystring == "") {
        return '?'+param+'='+paramValue;
    }else{
        querystring = querystring.replace("?","");
        //querystring = querystring.replace("#","");
        var queries = querystring.split('&');
        var outputquery = "?";
        $.each(queries,function(index,value){
            if (value.indexOf(param+"=") < 0) {
                if (value == "") {}else{
                    outputquery += "&"+value;
                }
            }
        });
        outputquery += '&'+param+'='+paramValue;
        return outputquery;
    }
}

//TinyMCE
function initTinyMCE(selector){
    tinymce.init({
        selector: selector,
        content_css: ['/bundles/rushframeworkbackoffice/admin/css/vendor.css', '/bundles/rushframeworkbackoffice/admin/css/app-blue.css', '/bundles/rushframeworkbackoffice/css/backoffice.css'],
        font_formats: 'Segoe UI=Segoe UI;' +'Andale Mono=andale mono,times;'+ 'Arial=arial,helvetica,sans-serif;'+ 'Arial Black=arial black,avant garde;'+ 'Book Antiqua=book antiqua,palatino;'+ 'Comic Sans MS=comic sans ms,sans-serif;'+ 'Courier New=courier new,courier;'+ 'Georgia=georgia,palatino;'+ 'Helvetica=helvetica;'+ 'Impact=impact,chicago;'+ 'Symbol=symbol;'+ 'Tahoma=tahoma,arial,helvetica,sans-serif;'+ 'Terminal=terminal,monaco;'+ 'Times New Roman=times new roman,times;'+ 'Trebuchet MS=trebuchet ms,geneva;'+ 'Verdana=verdana,geneva;'+ 'Webdings=webdings;'+ 'Wingdings=wingdings,zapf dingbats',
        height: 250,
        theme: 'modern',
        skin: 'custom',
        convert_urls:false,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'template paste textcolor colorpicker textpattern'
        ],
        toolbar: 'undo redo | styleselect | fontselect | fontsizeselect | forecolor backcolor | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | myGallery myFile media',
        setup: function (editor) {
            editor.addButton('myGallery', {
                text: 'My Gallery',
                icon: false,
                onclick: function () {
                    galleryChooser(editor);
                }
            });
            editor.addButton('myFile', {
                text: 'My File',
                icon: false,
                onclick: function () {
                    fileChooser(editor);
                }
            });
            editor.on('init', function()
            {
                this.getDoc().body.style.fontSize = '14px';
                this.getDoc().body.style.fontFamily = 'Segoe UI';
            });
        }
    });
}

// rf-ajaxify
function generateDialog(onclose){
    alertify.genericDialog || alertify.dialog('genericDialog',function(){
        return {
            main:function(content){
                this.setContent(content);
            },
            setup:function(){
                return {
                    focus:{
                        element:function(){
                            return this.elements.body.querySelector(this.get('selector'));
                        },
                        select:true
                    },
                    options:{
                        basic:true,
                        maximizable:true,
                        resizable:false,
                        padding:false
                    }
                };
            },
            settings:{
                selector:undefined
            },
            hooks: {
                onclose:function(){
                    if (onclose == undefined) {
                        // do nothing
                    }
                    if (onclose == "reload") {
                        location.reload(true);
                    }
                }
            }
        };
    });
    alertify.genericDialog ('<div class="rf-ajaxify-container"><div class="rush_framework_loader"></div></div>');
}