<?php

namespace RushFramework\BackOfficeBundle\EventSubscriber;

use RushFramework\CoreBundle\Event\MenuEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BackOfficeMenuSubscriber implements EventSubscriberInterface
{

    public function updateMenu(MenuEvent $menuEvent)
    {
        /**
         * you can modify the menu object here
         */
        $menuObject = $menuEvent->getMenuObject();

    }


    public static function getSubscribedEvents()
    {
        return array(
            MenuEvent::LOAD => 'updateMenu'
        );
    }

}