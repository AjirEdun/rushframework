<?php

namespace RushFramework\BackOfficeBundle\Controller;

use FM\ElfinderBundle\Form\Type\ElFinderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class GalleryController
 * @package RushFramework\BackOfficeBundle\Controller
 * @Route("/media")
 */
class MediaController extends BackOfficeBaseController
{
    /**
     * @Route("/gallery", name="rush_framework_backoffice.media.dashboard.gallery")
     */
    public function dashboardGalleryAction(Request $request)
    {
        $this->proceed("DASHBOARD GALLERY", "GALLERY", "Gallery Dashboard");
        $this->activeMenu('rf-core-dashboard-gallery');

        $form = $this->createFormBuilder()
            ->add(
                'manage',
                ElFinderType::class,
                array(
                    'instance'=>'gallery',
                    'enable'=>true ,
                    'attr'=>array(
                        'readOnly'=>'true',
                        'placeholder'=> ' Click Here',
                        'class' => 'hide'
                    )
                )
            )
            ->getForm();


        return $this->render('@RushFrameworkBackOffice/Media/dashboardGallery.html.twig', array(
            'data' => $this->data,
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/files", name="rush_framework_backoffice.media.dashboard.files")
     */
    public function dashboardFilesAction(Request $request)
    {
        $this->proceed("DASHBOARD FILES", "FILES", "Files Dashboard");
        $this->activeMenu('rf-core-dashboard-files');

        $form = $this->createFormBuilder()
            ->add(
                'manage',
                ElFinderType::class,
                array(
                    'instance'=>'files',
                    'enable'=>true ,
                    'attr'=>array(
                        'readOnly'=>'true',
                        'placeholder'=> ' Click Here',
                        'class' => 'hide'
                    )
                )
            )
            ->getForm();

        return $this->render('@RushFrameworkBackOffice/Media/dashboardFiles.html.twig', array(
            'data' => $this->data,
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/tiny-files", name="rush_framework_backoffice.media.tiny.files")
     * @Template()
     */
    public function tinyFilesAction(Request $request)
    {
        $this->proceed("DOWNLOAD FILES", "PUBLIC", "Download File");

        $form = $this->createFormBuilder( null, array ( 'attr' => array ( 'name' => 'rush-file-chooser') ) )
            ->add(
                'manageFile',
                ElFinderType::class,
                array(
                    'label' => "Choose File",
                    'instance'=>'files',
                    'enable'=>true ,
                    'attr'=>array(
                        'readOnly'=>'true',
                    )
                )
            )
            ->getForm();

        return  array(
            'data' => $this->data,
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/tiny-gallery", name="rush_framework_backoffice.media.tiny.gallery")
     * @Template()
     */
    public function tinyGalleryAction(Request $request)
    {
        $this->proceed("DOWNLOAD GALLERY", "PUBLIC", "Add Images or Videos");

        $form = $this->createFormBuilder( null, array ( 'attr' => array ( 'name' => 'rush-file-chooser') ) )
            ->add(
                'manageGallery',
                ElFinderType::class,
                array(
                    'label' => "Choose Image or Video",
                    'instance'=>'gallery',
                    'enable'=>true ,
                    'attr'=>array(
                        'readOnly'=>'true',
                    )
                )
            )
            ->getForm();

        return  array(
            'data' => $this->data,
            'form' => $form->createView()
        );
    }



    /**
     * @param Request $request
     * @return Response|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/download", name="rush_framework_backoffice.media.files.download")
     */
    public function downloadFileAction(Request $request)
    {
        $this->proceed("DOWNLOAD FILES","PUBLIC");

        $relPath = $request->get("form_path");

        $path = $this->get("kernel")->getRootDir(). str_replace('/../app','',$relPath);

        $path = urldecode($path);

        if( file_exists($path) ) {
            $fileExplode = explode(".",$path);
            $ind = count($fileExplode)-1;
            $filex = $fileExplode[$ind];
            $response = new Response(file_get_contents($path));
            $d = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, "download.".$filex);
            $response->headers->set('Content-Disposition', $d);
            return $response;
        }

        return $this->redirectToRoute('rush_framework_core.error.access',array('error_message'=>'The file you are trying to download is inaccessible.'));

    }

    /**
     * @param Request $request
     * @return array
     * @Route("/get", name="rush_framework_backoffice.media.gallery.source")
     * @Template()
     */
    public function getFileAction(Request $request)
    {
        $root = $this->get("kernel")->getRootDir()."/../web";
        $relPath = $request->get("relPath");
        $relPath = urldecode($relPath);

        $small = $request->get("small");
        $medium = $request->get("medium");
        $large = $request->get("large");

        $path = $root . $relPath;

        $src= "";

        $type = null;
        if( file_exists($path) ) {
            $splInfo = new \SplFileInfo($path);
            $extension = $splInfo->getExtension();

            //$src = $this->get('assets.packages')->getUrl($relPath);
            if ('/' == substr($relPath,0,1)) {
                $src = substr($relPath,1);
            } else {
                $src = $relPath;
            }


            if (in_array($extension, array('jpg','jpeg','png','gif','giff', 'jpe', 'tiff'))) {
                $type = 'image';
            } elseif (in_array($extension, array('flv', 'avi', 'mpeg', 'mp4'))) {
                $type = 'video';
            }
        }

        return array(
            'type'=>$type,
            'src'=>$src,
            'large' => $large,
            'medium' => $medium,
            'small' => $small
        );
    }

}