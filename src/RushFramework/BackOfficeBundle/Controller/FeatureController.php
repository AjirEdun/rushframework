<?php

namespace RushFramework\BackOfficeBundle\Controller;

use RushFramework\CoreBundle\Entity\Feature;
use RushFramework\CoreBundle\Form\FeatureType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FeatureController
 * @package RushFramework\BackOfficeBundle\Controller
 * @Route("/feature")
 */
class FeatureController extends BackOfficeBaseController
{
    /**
     * @Route("/", name="rush_framework_backoffice.feature.dashboard")
     */
    public function dashboardAction(Request $request)
    {
        $this->proceed("PUBLIC", "PUBLIC", "role Dashboard");
        $this->activeMenu('rf-core-dashboard-feature');

        $dashboard = array(
            'title'=>"Feature Dashboard",
            'box' => array(
                array(
                    'name' => 'Add Feature',
                    'description' => 'Add A Feature',
                    'route' => 'rush_framework_backoffice.feature.add',
                    'params' => array(),
                    'icon' => 'fa  fa-plus'
                ),
                array(
                    'name' => 'View Features',
                    'description' => 'View or Edit Your Features',
                    'route' => 'rush_framework_backoffice.feature.list',
                    'params' => array(),
                    'icon' => 'fa  fa-list'
                ),
            )
        );

        $this->data['content'] = $dashboard;

        return $this->renderDashboard($request);
    }

    /**
     * @Route("/add", name="rush_framework_backoffice.feature.add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $this->proceed("Add Feature","Vital");
        $this->activeMenu('rf-core-dashboard-feature');

        $feature = new Feature();
        $form = $this->createForm(FeatureType::class, $feature);
        $form->add('save', SubmitType::class, array('label' => 'Add Feature','attr' => array("class"=>"btn btn-block btn-primary")));

        $groupings = $this->getFeatureService()->getFeatureGroupings();

        $form->handleRequest($request);
        if ($form->isValid()) {
            /**
             * @var Feature $feature
             */
            $feature = $form->getData();

            //CHECKING IF FEATURE NAME WITH GROUP ALREADY IN USE
            if ($this->getFeatureService()->addFeature($feature)) {
                return $this->redirectToRoute("rush_framework_backoffice.feature.list",array('messages_success'=>"Feature Added Successfully"));

            }else{
                $form->addError(new FormError('Feature Name Already Exist For This Group.'));
                $this->data['messages']['error'] = "Validation Error";
            }
        }

        return array(
            'data' => $this->data,
            'form'=>$form->createView(),
            'groupings' => $groupings
        );
    }

    /**
     * @Route("/list", name="rush_framework_backoffice.feature.list")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $this->proceed("VIEW FEATURE","VITAL");
        $this->activeMenu('rf-core-dashboard-feature');
        $params = array();

        $params['title'] = "Feature List";
        $params['description'] = "You can edit, delete or view all your features from here";

        $grouping = $this->getFeatureService()->getAllFeaturesByGrouping();
        ksort($grouping);

        return array(
            'data' => $this->data,
            'grouped' => $grouping
        );
    }

    /**
     * @Route("/view/{id}", name="rush_framework_backoffice.feature.view")
     * @Template()
     */
    public function viewAction(Request $request, $id)
    {
        $this->proceed("View Feature","VITAL");

        $feature = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->find($id);
        if ($feature == null) {
            return $this->redirectToRoute('rush_framework_core.error.access',array('error_message'=> "You are trying to access an invalid Role."));
        }



        return array(
            'data' => $this->data,
            'feature' => $feature
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("role_ajax_turn_feature_off/{id}", name="rush_framework_backoffice.feature.turn.off")
     */
    public function ajaxTurnFeatureOff(Request $request, $id)
    {
        $toReturn = "rf-true";

        $this->proceed("Edit Feature","VITAL");
        if (!$this->masterData['access']['right']) {
            $toReturn = "false";
        }else{

            $feature = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->find($id);
            if ($feature == null) {
                $toReturn = "false";
            }

            if(!$this->getFeatureService()->disactivateFeature($feature)){
                $toReturn = "false";
            }
        }
        return new Response($toReturn);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("role_ajax_turn_feature_on/{id}", name="rush_framework_backoffice.feature.turn.on")
     */
    public function ajaxTurnFeatureOn(Request $request, $id)
    {
        $toReturn = "rf-true";

        $this->proceed("Edit Feature","VITAL");
        if (!$this->masterData['access']['right']) {
            $toReturn = "false";
        }else{

            $feature = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->find($id);
            if ($feature == null) {
                $toReturn = "false";
            }

            if(!$this->getFeatureService()->activateFeature($feature)){
                $toReturn = "false";
            }
        }
        return new Response($toReturn);
    }

}