<?php

namespace RushFramework\BackOfficeBundle\Controller;

use RushFramework\CoreBundle\Objects\PaginationObject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BackOfficeBaseController
{
    /**
     * @Route("/dashboard", name="rush_framework_backoffice.root.dashboard")
     * @Template()
     */
    public function dashboardAction(Request $request)
    {
        $this->proceed("DASHBOARD", "BACK OFFICE", "Rush Dashboard");

        if (!$request->isXmlHttpRequest()) {
            return $this->redirectToRoute('rush_framework_backoffice.root');
        }

        return array (
            'data' => $this->data
        );
    }

    /**
     * @Route("/", name="rush_framework_backoffice.root")
     */
    public function indexAction()
    {
        $this->proceed("DASHBOARD", "BACK OFFICE", "Rush Dashboard");
        $this->activeMenu("rf-root-dashboard");

        return $this->render('RushFrameworkBackOfficeBundle:Default:index.html.twig', array(
            'data' => $this->data
        ));
    }

    /**
     * @param Request $request
     * @return array
     * @Route("/tiny", name="tiny")
     * @Template()
     */
    public function tinyAction(Request $request)
    {
        $this->proceed("PUBLIC","PUBLIC");

        $form = $this->createFormBuilder()
            ->add('textOne',TextareaType::class,array('label'=>'TextOne','required'=>false, 'attr'=>array('class'=>'form-control textarea')))
            ->add('save', SubmitType::class, array('label' => 'Create','attr' => array("class"=>"btn btn-block btn-primary")))
            ->getForm();

        $finish = false;
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $finish= $data['textOne'];

        }

        return array(
            'data' => $this->data,
            'form' => $form->createView(),
            'finish' => $finish
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/pagination", name="paginationAndFilterTest")
     */
    public function paginationAction(Request $request)
    {
        $this->proceed("PUBLIC","PUBLIC");

        $params = array();

        $params['title'] = "Feature List";
        $params['description'] = "You can edit, delete or view all your roles from here";

        /**
         * PAGINATION
         */
        $countRoles = intval($this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->createQueryBuilder('f')
            ->select("count(f)")->getQuery()->getResult()[0][1]);

        $ps = $this->getPaginationService();
        $pagination = $ps->init(
            $countRoles,
            $request->get(PaginationObject::URL_KEY),
            $request->get(PaginationObject::URL_NUM_KEY),
            3
        );

        $roles = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->findBy(
            array(),
            array('name' => 'ASC'),
            $pagination->getNumberPerPage(),
            $pagination->getOffset()
        );

        $params['pagination']=$pagination;
        /**
         * END OF PAGINATION
         */

        $params['table']['headers'] = array('Features', 'Group', 'Actions');
        $roleList = array();

        foreach ($roles as $role) {
            $list = array();
            $list[] = $role->getName();
            $list[] = $role->getGrouping();
            $list[] = array(
                'viewId' => $role->getId(),
                'viewLink' => 'rush_framework_backoffice.feature.view',
                'editId' => null,
                'editLink' => null,
                'deleteId' => null,
                'deleteLink' => null
            );

            $roleList[] = $list;
        }

        $params['table']['list'] = $roleList;


        return $this->renderList($request, $params);
    }

}
