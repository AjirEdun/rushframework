<?php

namespace RushFramework\BackOfficeBundle\Controller;
use RushFramework\CoreBundle\Constants\NOTE_COMPONENT;
use RushFramework\CoreBundle\Entity\Role;
use RushFramework\CoreBundle\Form\RoleType;
use RushFramework\CoreBundle\Objects\PaginationObject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class RoleController
 * @package RushFramework\BackOfficeBundle\Controller
 * @Route("/role")
 */
class RoleController extends BackOfficeBaseController
{
    /**
     * @Route("/", name="rush_framework_backoffice.role.dashboard")
     */
    public function dashboardAction(Request $request)
    {
        $this->proceed("DASHBOARD ROLE", "ROLE", "Role Dashboard");
        $this->activeMenu('rf-core-dashboard-role');

        $dashboard = array(
            'title'=>"Role Dashboard",
            'box' => array(
                array(
                    'name' => 'Add Role',
                    'description' => 'Add A Role',
                    'route' => 'rush_framework_backoffice.role.add',
                    'params' => array(),
                    'icon' => 'fa  fa-plus'
                ),
                array(
                    'name' => 'View Roles',
                    'description' => 'View or Edit your roles',
                    'route' => 'rush_framework_backoffice.role.list',
                    'params' => array(),
                    'icon' => 'fa  fa-list',
                    'ajax' => true
                ),
            )
        );

        $this->data['content'] = $dashboard;

        return $this->renderDashboard($request);
    }

    /**
     * @Route("/add", name="rush_framework_backoffice.role.add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $this->proceed("ADD ROLE","ROLE");

        $role = new Role();
        $role->setProfileSet(1);
        $form = $this->createForm(RoleType::class, $role);
        $form->add('save', SubmitType::class, array('label' => 'Add Role','attr' => array("class"=>"btn btn-block btn-primary")));

        $form->handleRequest($request);
        if ($form->isValid()) {
            /**
             * @var Role $role
             */
            $role = $form->getData();

            //Check if role exist
            if($this->getRoleService()->addRole($role)){
                return $this->redirectToRoute("rush_framework_backoffice.role.list",array('messages_success'=>"Role Added Successfully"));
            }else{
                $form->addError(new FormError('Role Name Already Exist'));
                $this->data['messages']['error'] = "Validation Error";
            }

        }

        return array(
            'data' => $this->data,
            'form'=>$form->createView()
        );
    }


    /**
     * @Route("/list", name="rush_framework_backoffice.role.list")
     */
    public function listAction(Request $request)
    {
        $access = $this->proceed("VIEW ROLE","ROLE");
        $this->activeMenu('rf-core-dashboard-role');
        $params = array();

        $params['title'] = "Role List";
        $params['description'] = "You can edit, delete or view all your roles from here";

        /**
         * PAGINATION
         */
        $countRoles = intval($this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Role")->createQueryBuilder('f')
            ->select("count(f)")->getQuery()->getResult()[0][1]);

        $ps = $this->getPaginationService();
        $pagination = $ps->init(
            $countRoles,
            $request->get(PaginationObject::URL_KEY),
            $request->get(PaginationObject::URL_NUM_KEY),
            3
        );

        $roles = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Role")->findBy(
            array(),
            array('name' => 'ASC'),
            $pagination->getNumberPerPage(),
            $pagination->getOffset()
        );

        $params['pagination']=$pagination;
        /**
         * END OF PAGINATION
         */

        $params['table']['headers'] = array('Roles', 'Profile Set', 'Number of Users', 'Actions');
        $roleList = array();

        foreach ($roles as $role) {
            $list = array();
            $list[] = $role->getName();
            $list[] = $role->getProfileSet();
            $list[] = count($role->getUsers());
            $list[] = array(
                'viewId' => $role->getId(),
                'viewLink' => 'rush_framework_backoffice.role.view',
                'editId' => null,
                'editLink' => null,
                'deleteId' => null,
                'deleteLink' => null
            );

            $roleList[] = $list;
        }

        $params['table']['list'] = $roleList;

        return $this->renderList($request, $params);
    }

    /**
     * @Route("/view/{id}", name="rush_framework_backoffice.role.view")
     * @Template()
     */
    public function viewAction(Request $request, $id)
    {
        $this->proceed("VIEW ROLE","ROLE");
        $this->activeMenu('rf-core-dashboard-role');

        $role = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Role")->find($id);
        if ($role == null) {
            return $this->redirectToRoute('rush_framework_core.error.access',array('error_message'=> "You are trying to access an invalid Role."));
        }

        $grouped = $this->getRoleService()->getFeaturesByRoleGroupByGrouping($role);

        $role_number = count($this->getRoleService()->getFeatureIdsByRole($role));

        $users = $role->getUsers();

        return array(
            'data' => $this->data,
            'role' => $role,
            'grouped' => $grouped,
            'role_number' => $role_number,
            'users' => $users,
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @param $featureId
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("ajax_feature_enable/{id}/{featureId}", name="rush_framework_backoffice.role.feature.enable")
     */
    public function ajaxEnableFeatureForRole(Request $request, $id, $featureId)
    {
        $toReturn = "rf-true";

        $this->proceed("ACTIVATE FEATURE FOR ROLE","ROLE");
        if (!$this->masterData['access']['right']) {
            $toReturn = "false";
        }else{
            $role = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Role")->find($id);
            if ($role == null) {
                $toReturn = "false";
            }

            $feature = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->find($featureId);
            if ($feature == null) {
                $toReturn = "false";
            }

            if(!$this->getFeatureService()->enableFeatureForRole($feature,$role)){
                $toReturn = "false";
            }
        }
        return new Response($toReturn);
    }

    /**
     * @param Request $request
     * @param $id
     * @param $featureId
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("ajax_feature_disable/{id}/{featureId}", name="rush_framework_backoffice.role.feature.disable")
     */
    public function ajaxDisableFeatureForRole(Request $request, $id, $featureId)
    {
        $toReturn = "rf-true";

        $this->proceed("DESACTIVATE FEATURE FOR ROLE","ROLE");
        if (!$this->masterData['access']['right']) {
            $toReturn = "false";
        }else{
            $role = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Role")->find($id);
            if ($role == null) {
                $toReturn = "false";
            }

            $feature = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->find($featureId);
            if ($feature == null) {
                $toReturn = "false";
            }

            if(!$this->getFeatureService()->disableFeatureForRole($feature,$role)){
                $toReturn = "false";
            }
        }
        return new Response($toReturn);

    }


    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Template()
     * @Route("/change-role/{id}", name="rush_framework_backoffice.role.change")
     */
    public function changeRoleAction(Request $request, $id)
    {
        $this->proceed("CHANGE USER ROLE","ROLE");

        $success= false;

        if ($user = $this->getUserService()->getUser($id)) {
            $role = $user->getRole();

            $form = $this->createFormBuilder(null,array('action' => $this->generateUrl('rush_framework_backoffice.role.change',array('id'=>$id)),'attr'=>array('class'=>'rf-ajaxify-form')))->getForm();
            $form
                ->add('selection',EntityType::class,array(
                    'label'=>'Choose Role',
                    'required'=>true,
                    'class'=>'RushFramework\CoreBundle\Entity\Role',
                    'choice_label'=> 'name'
                ))
            ;
            $form->add('save', SubmitType::class, array('label' => 'Change','attr' => array("class"=>"btn btn-block btn-primary")));

            $form->get('selection')->setData($role);

            $form->handleRequest($request);
            if ($form->isValid()) {
                $beforeRole = $user->getRole()->getName();
                $role = $form->get('selection')->getData();
                $user->setRole($role);
                $this->getEntityManager()->flush();
                $success = true;
                $noteService = $this->getNoteService();
                $noteService->sendComponentToUser(NOTE_COMPONENT::ROLE_MANAGER, $user,
                    $this->getUserService()->getCurrentUser()->getName()." has changed your role from ". $beforeRole." to ". $user->getRole()->getName() ." ");
            }

            return array(
                'data' => $this->data,
                'success' => $success,
                'form' => $form->createView()
            );
        }else{
            return $this->redirectToRoute('rush_framework_core.error.access',array('error_message'=>'Invalid User'));
        }
    }


}