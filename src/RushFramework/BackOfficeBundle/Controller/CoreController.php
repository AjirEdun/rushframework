<?php

namespace RushFramework\BackOfficeBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CoreController
 * @package RushFramework\BackOfficeBundle\Controller
 * @Route("/core")
 */
class CoreController extends BackOfficeBaseController
{
    /**
     * @Route("/", name="rush_framework_backoffice.core.dashboard")
     */
    public function dashboardAction(Request $request)
    {
        $this->proceed("PUBLIC", "PUBLIC", "role Dashboard");
        $this->activeMenu('rf-core-dashboard');
        $dashboard = array(
            'title'=>"Core Dashboard",
            'description'=>'This section allows you to manage all the core features of this application.',
            'box' => array(
                array(
                    'name' => 'User Management',
                    'description' => 'Manage all your users',
                    'route' => 'rush_framework_backoffice.user.dashboard',
                    'params' => array(),
                    'icon' => 'fa  fa-user'
                ),
                array(
                    'name' => 'Role Management',
                    'description' => 'Manage all your roles',
                    'route' => 'rush_framework_backoffice.role.dashboard',
                    'params' => array(),
                    'icon' => 'fa  fa-tags'
                ),
                array(
                    'name' => 'Feature Management',
                    'description' => "Manage your application's features",
                    'route' => 'rush_framework_backoffice.feature.dashboard',
                    'params' => array(),
                    'icon' => 'fa  fa-sliders'
                ),
            )
        );

        $this->data['content'] = $dashboard;

        return $this->renderDashboard($request);
    }
}