<?php

namespace RushFramework\BackOfficeBundle\Controller;

use RushFramework\CoreBundle\Entity\User;
use RushFramework\CoreBundle\Form\UserSignUpType;
use RushFramework\CoreBundle\Objects\PaginationObject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package RushFramework\BackOfficeBundle\Controller
 * @Route("/user")
 */
class UserController extends BackOfficeBaseController
{

    /**
     * @Route("/", name="rush_framework_backoffice.user.dashboard")
     */
    public function dashboardAction(Request $request)
    {
        $this->proceed("PUBLIC", "PUBLIC", "role Dashboard");
        $this->activeMenu('rf-core-dashboard-user');

        $dashboard = array(
            'title'=>"User Dashboard",
            'description'=>'This section allows you to manage all the users in your application',
            'box' => array(
                array(
                    'name' => 'Add A User',
                    'description' => "Add a user",
                    'route' => 'rush_framework_backoffice.user.add',
                    'params' => array(),
                    'icon' => 'fa  fa-plus'
                ),
                array(
                    'name' => 'View Users',
                    'description' => 'View and manage users',
                    'route' => 'rush_framework_backoffice.user.list',
                    'params' => array(),
                    'icon' => 'fa  fa-list'
                ),
            )
        );

        $this->data['content'] = $dashboard;

        return $this->renderDashboard($request);
    }

    /**
     * @Route("/list", name="rush_framework_backoffice.user.list")
     */
    public function viewAllAction(Request $request)
    {
        $this->proceed("VIEW USER","USER MANAGEMENT");
        $this->activeMenu('rf-core-dashboard-user');

        $params = array();

        $params['title'] = "User List";
        $params['description'] = "You can edit, delete or view all your users from here";


        /**
         * PAGINATION
         */
        $countUsers = intval($this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User")->createQueryBuilder('f')
            ->select("count(f)")->getQuery()->getResult()[0][1]);

        $ps = $this->getPaginationService();
        $pagination = $ps->init(
            $countUsers,
            $request->get(PaginationObject::URL_KEY),
            $request->get(PaginationObject::URL_NUM_KEY),
            3
        );

        $users = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User")->findBy(
            array(),
            array('surname' => 'ASC'),
            $pagination->getNumberPerPage(),
            $pagination->getOffset()
        );

        $params['pagination']=$pagination;
        /**
         * END OF PAGINATION
         */

        $params['table']['headers'] = array('Surname', 'Name', 'Email', 'Role', 'Actions');
        $roleList = array();

        foreach ($users as $user) {
            $list = array();
            $list[] = $user->getSurname();
            $list[] = $user->getName();
            $list[] = $user->getEmail();
            $list[] = $user->getRole()->getName();
            $list[] = array(
                'viewId' => $user->getId(),
                'viewLink' => 'rush_framework_backoffice.user.view',
                'editId' => null,
                'editLink' => null,
                'deleteId' => null,
                'deleteLink' => null
            );

            $roleList[] = $list;
        }

        $params['table']['list'] = $roleList;

        return $this->renderList($request, $params);
    }

    /**
     * @Route("/view/{id}", name="rush_framework_backoffice.user.view")
     * @Template()
     */
    public function viewAction(Request $request, $id)
    {
        $this->proceed("VIEW USER","USER MANAGEMENT");
        $this->activeMenu('rf-core-dashboard-user');

        $user = $this->getUserService()->getUser($id);
        if ($user == null) {
            return $this->redirectToRoute('rush_framework_core.error.access',array('error_message'=> "Sorry, this user does not exist"));
        }

        return array(
            'data' => $this->data,
            'user' => $user
        );
    }

    /**
     * @Route("/add", name="rush_framework_backoffice.user.add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $this->proceed("CREATE USER","USER MANAGEMENT");
        $this->activeMenu('rf-core-dashboard-user');

        $user = new User();
        $user->setSugar($this->getUserService()->generateSugar());
        $form = $this->createForm(UserSignUpType::class, $user);
        $form->add('role', EntityType::class, array(
            'label'=>'Choose Role',
            'required'=>true,
            'class'=>'RushFramework\CoreBundle\Entity\Role',
            'choice_label'=> 'name'
        ));
        $form->add('save', SubmitType::class, array('label' => 'Create','attr' => array("class"=>"btn btn-block btn-primary")));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $user = $form->getData();

            if (null === $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User")->findOneByEmail($user->getEmail())) {
                if (!is_null($form->get('name')->getData())) {
                    if (!is_null($form->get('surname')->getData())) {
                        if ($user->getPassword() == $form->get('confirmPassword')->getData()) {

                            /**
                             * @var UploadedFile $file
                             */
                            $fileError = false;
                            $hasProfilePicture = false;
                            $file = $form->get('image')->getData();
                            if ($file) {
                                if ($file->getFileInfo()->getSize() < 1010000) {
                                    $fileMime = strtolower($file->getClientMimeType());
                                    if ($fileMime == "image/png" || $fileMime == "image/jpg" || $fileMime == "image/gif" || $fileMime == "image/jpeg") {
                                        $hasProfilePicture = true;
                                    }else{
                                        $form->addError(new FormError('File Uploaded is not an image. Please upload the following format: .png .jpg .gif'));
                                        $access['data']['messages']['error'] = "Validation Error";
                                        $fileError = true;
                                    }
                                }else{
                                    $form->addError(new FormError('Image size is too big, maximum size is 1Mb'));
                                    $access['data']['messages']['error'] = "Validation Error";
                                    $fileError = true;
                                }
                            }
                            if (!$fileError) {
                                //Password
                                $user = $this->getUserService()->setPassword($user, $user->getPassword());

                                //Generate Activation Link
                                $user->setActivationHash(md5(rand(1,5000)));

                                //SEND ACTIVATION EMAIL
                                //TODO
                                $this->getEntityManager()->persist($user);
                                $this->getEntityManager()->flush();

                                if ($hasProfilePicture) {
                                    $fileName = "profile_picture.".$file->getClientOriginalExtension();
                                    $path = "/data/users/".$user->getId()."/";
                                    $fullPath = $this->get("kernel")->getRootDir(). $path;

                                    $file->move($fullPath,$fileName);

                                    $user->setImage($path.$fileName);

                                    //copy image in the web folder
                                    $fs = new Filesystem();
                                    $fs->copy($fullPath.$fileName,$this->get("kernel")->getRootDir().'/../web'.$path.$fileName);
                                } else {
                                    $fileName = "profile_picture.png";
                                    $path = "/data/users/".$user->getId()."/";
                                    $fullPath = $this->get("kernel")->getRootDir(). $path;

                                    $fs = new Filesystem();
                                    $fs->copy(
                                        $this->get("kernel")->getRootDir().'/../web/bundles/rushframeworkbackoffice/images/profile_picture.png',
                                        $fullPath.$fileName
                                    );

                                    $user->setImage($path.$fileName);
                                    //copy image in the web folder
                                    $fs = new Filesystem();
                                    $fs->copy(
                                        $this->get("kernel")->getRootDir().'/../web/bundles/rushframeworkbackoffice/images/profile_picture.png',
                                        $this->get("kernel")->getRootDir().'/../web'.$path.$fileName
                                    );
                                }
                                $this->getEntityManager()->flush();

                                $fs = new Filesystem();
                                $userId = $user->getId();
                                $fs->mkdir($this->get('kernel')->getRootDir().'/data/users/'.$userId,0777);

                                $return = $this->postCreation($user);

                                if (is_null($return)) {
                                    $html = "<a href='".$this->generateUrl('activate',array('email'=>$user->getEmail(),'hash'=>$user->getActivationHash()))."'>Activate</a>";

                                    return new Response($html);
                                } else {

                                    return $return;
                                }
                            }
                        }else{
                            $form->get('password')->addError(new FormError('Passwords are not matching'));
                            $access['data']['messages']['error'] = "Validation Error";
                        }
                    }else{
                        $form->get('name')->addError(new FormError('Surname should not be blank'));
                        $access['data']['messages']['error'] = "Validation Error";
                    }
                }else{
                    $form->get('name')->addError(new FormError('Name should not be blank'));
                    $access['data']['messages']['error'] = "Validation Error";
                }
            }else{
                $form->get('email')->addError(new FormError('Email already exist'));
                $access['data']['messages']['error'] = "Validation Error";
            }
        }
        return array(
            'data' => $this->data,
            'form'=>$form->createView()
        );
    }

    /**
     * What to do after creating the user
     */
    protected function postCreation(User $user)
    {
        return $this->redirectToRoute('rush_framework_backoffice.user.list',array('success_message'=> "User Created Successfully"));
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     * @Route("/profile_picture/{id}", name="rush_framework_backoffice.user.profile_picture")
     * @Template()
     */
    public function profilePictureAction(Request $request,$id)
    {
        $access = $this->proceed("PROFILE PICTURE","USER");

        $currentUser = $this->getUserService()->getCurrentUser();
        if ( $currentUser === null ) {
            return $this->redirectToRoute('rush_framework_core.error.access', array('error_message'=>"You cannot change the profile picture of this user"));
        }

        if ($currentUser->getId() != $id) {
            if (!$this->currentUserHasAccessTo("CHANGE USER PROFILE PICTURE")) {

                return $this->redirectToRoute('rush_framework_core.error.access',array('error_message'=>"You don't have access to perform this task"));
            }
        }

        $form = $this->createFormBuilder()->getForm()
            ->add('file', FileType::class, array('label'=>'Profile Picture :', 'required'=>true, 'data_class' => null));
        $form->add('save', SubmitType::class, array('label' => 'Upload', 'attr' => array("class"=>"btn btn-primary")));

        $form->handleRequest($request);
        if ($form->isValid()) {
            /**
             * @var UploadedFile $file
             */
            $file = $form->get('file')->getData();
            if ($file->getFileInfo()->getSize() < 1010000) {
                $fileMime = strtolower($file->getClientMimeType());
                if ($fileMime == "image/png" || $fileMime == "image/jpg" || $fileMime == "image/gif" || $fileMime == "image/jpeg") {
                    $fileName = "profile_picture.".$file->getClientOriginalExtension();

                    //getting the user whoe image is to change
                    $user = $this->getUserService()->getUser($id);

                    if (is_null($user)) {
                        throw new \Exception("Invalid User Id");
                    }


                    $path = "/data/users/".$user->getId()."/";
                    $fullPath = $this->get("kernel")->getRootDir(). $path;

                    $file->move($fullPath,$fileName);

                    $user->setImage($path.$fileName);
                    $this->getEntityManager()->flush();

                    //copy image in the web folder
                    $fs = new Filesystem();
                    $webPath = $this->get("kernel")->getRootDir().'/../web'.$path.$fileName;
                    $fs->copy($fullPath.$fileName, $webPath);

                    //remove from cache
                    $cacheManager = $this->get('liip_imagine.cache.manager');
                    $cacheManager->remove($path.$fileName);

                    return $this->redirectToRoute("rush_framework_backoffice.user.list",array("id"=> $user->getId(), "messages_success" => "Profile Picture Changed"));
                }else{
                    $form->addError(new FormError('File Uploaded is not an image. Please upload the following format: .png .jpg .gif'));
                    $access['data']['messages']['error'] = "Validation Error";
                }
            }else{
                $form->addError(new FormError('Image size is too big, maximum size is 1Mb'));
                $access['data']['messages']['error'] = "Validation Error";
            }
        }

        return array(
            'data' => $this->data,
            'form'=>$form->createView()
        );
    }

}