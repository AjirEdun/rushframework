<?php

namespace RushFramework\BackOfficeBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EditController extends BackOfficeBaseController
{
    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/content/edit", name="rush_framework_core.content.edit")
     * @Template()
     */
    public function editTextAction(Request $request)
    {
        $this->proceed("EDIT WEBSITE CONTENT","CONTENT MANAGEMENT");

        if ($rel = $request->get('rel')) {

            $form = $this->createFormBuilder(null,array('action' => $this->generateUrl('rush_framework_core.content.edit').'?rel='.$rel))->getForm();
            $form
                ->add('content',TextareaType::class,array('label'=>'Edit The Content','required'=>false,'data_class' => null))
            ;
            $form->add('save', SubmitType::class, array('label' => 'Save','attr' => array("class"=>"btn btn-primary")));

            $path = $this->get('kernel')->getRootDir()."/../web/".$rel;
            $content = "";
            if (file_exists($path)) {
                $content = file_get_contents($path);
            }
            $form->get('content')->setData($content);


            $form->handleRequest($request);
            if ($form->isValid()) {

                $dirname = dirname($path);
                if (!is_dir($dirname))
                {
                    mkdir($dirname, 0755, true);
                }

                $file = fopen($path,'w') or $this->redirectToRoute('rush_framework_core.error.access',array('error_message'=>"Unable to open this file"));
                fwrite($file,$form->get('content')->getData());
                fclose($file);
                $this->data['messages']["success"] = "Successfully Saved";
            }

        }else{
            return $this->redirectToRoute('rush_framework_core.error.access',array('error_message'=>"No Link Specified"));
        }

        return array(
            'data' => $this->data,
            'rel' => $rel,
            'form' => $form->createView()
        );
    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/content/delete", name="rush_framework_core.content.delete")
     */
    public function deleteTextAction(Request $request)
    {
        $this->proceed("EDIT WEBSITE CONTENT","CONTENT MANAGEMENT");

        $output = "An Unintented Error Occured";

        $rel = null;
        $rel= $request->get('rel');
        if ($rel !== null) {

            $path = $this->get('kernel')->getRootDir()."/../web/".$rel;
            if (file_exists($path)) {
                $file = fopen($path,'w') or $output = "Unable to open this file";
                fwrite($file,"");
                fclose($file);
                $output = "success";
            }else{
                $output = "Invalid Content To Delete";
            }
        }

        return new Response($output);
    }
}