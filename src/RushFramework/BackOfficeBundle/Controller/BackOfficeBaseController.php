<?php

namespace RushFramework\BackOfficeBundle\Controller;


use RushFramework\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class BackOfficeBaseController extends BaseController
{

    /**
     * @param $featureName
     * @param $featureGrouping
     * @param String|null $featureTitle
     */
    public function proceed($featureName, $featureGrouping, $featureTitle = null)
    {
        parent::proceed($featureName, $featureGrouping, $featureTitle);

        //add the backoffice menu inside
        $this->data['back_office']['menu'] = $this->getMenuService()->getMenu("back_office");
        $this->data['back_office']['active_menu'] = "";
    }


    /**
     * @param String $active_menu
     */
    public function activeMenu($active_menu)
    {
        $this->data['back_office']['active_menu'] = $active_menu;
    }

    /**
     * Render the dashboard
     */
    protected function renderDashboard(Request $request)
    {

        if ($request->isXmlHttpRequest()) {
            return $this->render('RushFrameworkBackOfficeBundle:Common:dashboard.html.twig', array(
                'data' => $this->data
            ));
        } else {
            return $this->render('RushFrameworkBackOfficeBundle:Common:dashboardNoAjax.html.twig', array(
                'data' => $this->data
            ));
        }
    }

    /**
     * Render the dashboard
     *
     * @param Request $request
     * @param array $params
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderList(Request $request,array $params = array())
    {

        if ($request->isXmlHttpRequest()) {
            return $this->render('RushFrameworkBackOfficeBundle:Common:list.html.twig', array(
                'data' => $this->data,
                'params' => $params
            ));
        } else {
            return $this->render('RushFrameworkBackOfficeBundle:Common:listNoAjax.html.twig', array(
                'data' => $this->data,
                'params' => $params
            ));
        }
    }
}