<?php

namespace RushFramework\FrontOfficeBundle\Controller;


use RushFramework\BackOfficeBundle\Controller\BackOfficeBaseController;
use RushFramework\CoreBundle\Constants\ACCESS_METHOD;
use RushFramework\CoreBundle\Entity\User;
use RushFramework\CoreBundle\Form\UserSignUpType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FrontController extends BackOfficeBaseController
{
    /**
     * @param Request $request
     * @Route("signup", name="signup")
     * @Template()
     * @return array | response
     */
    public function signupAction(Request $request)
    {
        $this->proceed("SIGN UP","PUBLIC");

        $user = new User();
        $user->setSugar($this->getUserService()->generateSugar());
        $form = $this->createForm(UserSignUpType::class, $user);
        $form->add('save', SubmitType::class, array('label' => 'Sign Up','attr' => array("class"=>"btn btn-block btn-primary")));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $user = $form->getData();

            if (null === $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User")->findOneByEmail($user->getEmail())) {
                if (!is_null($form->get('name')->getData())) {
                    if (!is_null($form->get('surname')->getData())) {
                        if ($user->getPassword() == $form->get('confirmPassword')->getData()) {

                            /**
                             * @var UploadedFile $file
                             */
                            $fileError = false;
                            $hasProfilePicture = false;
                            $file = $form->get('image')->getData();
                            if ($file) {
                                if ($file->getFileInfo()->getSize() < 1010000) {
                                    $fileMime = strtolower($file->getClientMimeType());
                                    if ($fileMime == "image/png" || $fileMime == "image/jpg" || $fileMime == "image/gif" || $fileMime == "image/jpeg") {
                                        $hasProfilePicture = true;
                                    }else{
                                        $form->addError(new FormError('File Uploaded is not an image. Please upload the following format: .png .jpg .gif'));
                                        $access['data']['messages']['error'] = "Validation Error";
                                        $fileError = true;
                                    }
                                }else{
                                    $form->addError(new FormError('Image size is too big, maximum size is 1Mb'));
                                    $access['data']['messages']['error'] = "Validation Error";
                                    $fileError = true;
                                }
                            }
                            if (!$fileError) {
                                //Password
                                $user = $this->getUserService()->setPassword($user, $user->getPassword());

                                //SET USER ROLE
                                $roleName = $this->getParameter("rush_framework_core.user_creation_front_role");
                                if (null !== $role = $this->getRoleService()->getRoleByName($roleName)) {
                                    $user->setRole($role);
                                }else{
                                    return $this->redirectToRoute("rush_framework_core.error.access", array(
                                        'error_message'=> "Sorry there is a technical error. Please contact Site Admin."
                                    ));
                                }

                                //Generate Activation Link
                                $user->setActivationHash(md5(rand(1,5000)));


                                $this->getEntityManager()->persist($user);
                                $this->getEntityManager()->flush();

                                if ($hasProfilePicture) {
                                    $fileName = "profile_picture.".$file->getClientOriginalExtension();
                                    $path = "/data/users/".$user->getId()."/";
                                    $fullPath = $this->get("kernel")->getRootDir(). $path;

                                    $file->move($fullPath,$fileName);

                                    $user->setImage($path.$fileName);

                                    //copy image in the web folder
                                    $fs = new Filesystem();
                                    $fs->copy($fullPath.$fileName,$this->get("kernel")->getRootDir().'/../web'.$path.$fileName);
                                } else {
                                    $fileName = "profile_picture.png";
                                    $path = "/data/users/".$user->getId()."/";
                                    $fullPath = $this->get("kernel")->getRootDir(). $path;

                                    $fs = new Filesystem();
                                    $fs->copy(
                                        $this->get("kernel")->getRootDir().'/../web/bundles/rushframeworkbackoffice/images/profile_picture.png',
                                        $fullPath.$fileName
                                    );

                                    $user->setImage($path.$fileName);
                                    //copy image in the web folder
                                    $fs = new Filesystem();
                                    $fs->copy(
                                        $this->get("kernel")->getRootDir().'/../web/bundles/rushframeworkbackoffice/images/profile_picture.png',
                                        $this->get("kernel")->getRootDir().'/../web'.$path.$fileName
                                    );
                                }
                                $this->getEntityManager()->flush();

                                $fs = new Filesystem();
                                $userId = $user->getId();
                                $fs->mkdir($this->get('kernel')->getRootDir().'/data/users/'.$userId,0777);

                                $link = $this->generateUrl('activate',array('email'=>$user->getEmail(),'hash'=>$user->getActivationHash()));

                                $this->getEmailService()
                                    ->sendMailToAdmin(
                                        'New Sign Up',
                                        'RushFrameworkFrontOfficeBundle:Email/Admin:signup.html.twig',
                                        array('user'=>$user),
                                        array('from_alias'=>$this->getWebsiteInfo()['name'])
                                    );

                                $this->getEmailService()
                                    ->sendMail(
                                        $user->getEmail(),
                                        'You have signed up into our website',
                                        'RushFrameworkFrontOfficeBundle:Email:signup.html.twig',
                                        array(),
                                        array('from_alias'=>$this->getWebsiteInfo()['name'])
                                    );

                                $this->sendActivationMail($user, $link);

                                return $this->redirectToRoute('homepage', array('messages_success'=>'Please check your email to activate your account'));
                            }
                        }else{
                            $form->get('password')->addError(new FormError('Passwords are not matching'));
                            $access['data']['messages']['error'] = "Validation Error";
                        }
                    }else{
                        $form->get('name')->addError(new FormError('Surname should not be blank'));
                        $access['data']['messages']['error'] = "Validation Error";
                    }
                }else{
                    $form->get('name')->addError(new FormError('Name should not be blank'));
                    $access['data']['messages']['error'] = "Validation Error";
                }
            }else{
                $form->get('email')->addError(new FormError('Email already exist'));
                $access['data']['messages']['error'] = "Validation Error";
            }
        }
        return array(
            'data' => $this->data,
            'form'=>$form->createView()
        );
    }

    /**
     * @Route("/activate/{email}/{hash}", name="activate")
     */
    public function activateAction(Request $request, $email, $hash)
    {
        $access = $this->proceed("ACTIVATION","PUBLIC");

        if ($this->getUserService()->getCurrentUser() === null) {

            $this->get("session")->set("activeHash",$hash);
            $this->get("session")->set('activeEmail',$email);

        }else{
            return $this->redirectToRoute("homepage",array("messages_warning" => "You are already Logged In"));
        }

        return $this->redirectToRoute("login");
    }

    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/activate_resend",name="activate_resend")
     * @Template()
     */
    public function resendActivationAction(Request $request)
    {
        $access = $this->proceed("RESEND ACTIVATION","PUBLIC");

        if ($this->getUserService()->getCurrentUser() === null) {

            $form = $this->createFormBuilder()
                ->add('email',EmailType::class,array('label'=>'Email','required'=>true, 'attr'=>array('class'=>'form-control underlined', 'placeholder'=>'Email')))
                ->add('save', SubmitType::class, array('label' => 'Resend Activation Link','attr' => array("class"=>"btn btn-block btn-primary")))
                ->getForm();

            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();

                $userRepo = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User");
                /**
                 * @var User $user
                 */
                $user = $userRepo->findOneByEmail($data['email']);

                if (!is_null($user)) {
                    if (!$user->getActive()) {
                        $user->setActivationHash(md5(rand(1,10000)));

                        $this->getEntityManager()->flush();

                        $link = $this->generateUrl('activate',array('email'=>$user->getEmail(),'hash'=>$user->getActivationHash()));

                        $this->sendActivationMail($user, $link);

                        return $this->redirectToRoute("activate_resend",array("messages_success" => "Please check your email"));

                    }else{
                        $form->addError(new FormError("Your account has already been activated. Please log in"));
                    }
                }else{
                    $form->addError(new FormError("Sorry, there is no account with this email"));
                    $access['data']['messages']['error'] = "You can create a new account by Signing Up";
                }
            }

        }else{
            return $this->redirectToRoute("homepage",array("messages_warning" => "You are already Logged In"));
        }

        return array(
            'data' => $this->data,
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/login", name="login")
     * @Template()
     */
    public function loginAction(Request $request)
    {

        $this->proceed("LOGIN","PUBLIC");

        if ($this->getUserService()->getCurrentUser() === null) {

            $form = $this->createFormBuilder()
                ->add('email',EmailType::class,array('label'=>'Email','required'=>true, 'attr'=>array('class'=>'form-control underlined', 'placeholder'=>'Email')))
                ->add('password',PasswordType::class, array('label' => 'Password','required'=>true,  'attr'=>array('class'=>'form-control underlined', 'placeholder'=>'Password')))
                ->add('save', SubmitType::class, array('label' => 'Login','attr' => array("class"=>"btn btn-block btn-primary")))
                ->getForm();

            if ($email = $this->get("session")->get('activeEmail')) {
                $form->get('email')->setData($email);
            }

            $form->handleRequest($request);
            if ($form->isValid()) {
                $login = $form->getData();

                $userRepo = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User");
                /**
                 * @var User $user
                 */
                $user = $userRepo->findOneByEmail($login['email']);

                if (!is_null($user)) {
                    if ($user->getPassword() === $this->getUserService()->formatPassword($user, $login['password'])) {

                        if ($user->getActive()) {
                            $this->logUserIn($user);
                            $user->setLoginAttempt(0);
                            $this->getEntityManager()->flush();
                            //if we have a url in session
                            if ($this->masterData['access']['method'] == ACCESS_METHOD::REDIRECT) {
                                return $this->redirect($this->masterData['access']['url']);
                            } else {
                                return $this->redirectToRoute("homepage",array("messages_success"=> "Welcome !"));
                            }
                        }else{
                            $activeHash = $this->get("session")->get("activeHash");
                            if ($activeHash){
                                if ($activeHash == $user->getActivationHash()) {
                                    $this->logUserIn($user);
                                    $this->get("session")->remove('activeEmail');
                                    $this->get("session")->remove("activeHash");
                                    $user->setActive(true);
                                    $user->setLoginAttempt(0);
                                    $this->getEntityManager()->flush();

                                    //if we have a url in session
                                    if ($this->masterData['access']['method'] == ACCESS_METHOD::REDIRECT) {
                                        return $this->redirect($this->masterData['access']['url']);
                                    } else {
                                        return $this->getUserRedirection($user);
                                    }
                                }else{
                                    $form->addError(new FormError('Invalid Activation, Try to resend activation mail'));
                                    $access['data']['messages']['error'] = "Invalid Account Activation";
                                }
                            }else{
                                $form->addError(new FormError('You need to activate your account first.'));
                                $access['data']['messages']['error'] = "You need to activate your account";
                            }
                        }
                    }else{
                        $user->setLoginAttempt($user->getLoginAttempt()+1);
                        $this->getEntityManager()->flush();
                        if ($user->getLoginAttempt()>3) {
                            $form->addError(new FormError('You have tried more than three times, Please Click On Forget Password'));
                            $access['data']['messages']['error'] = "Validation Error";
                        }else{
                            $form->addError(new FormError('Invalid Email and Password'));
                            $access['data']['messages']['error'] = "Validation Error";
                        }
                    }
                }else{
                    $form->addError(new FormError('Invalid Email and Password'));
                    $access['data']['messages']['error'] = "Validation Error";
                }
            }
        }else{
            return $this->redirectToRoute("homepage",array("messages_warning" => "You are already Logged In"));
        }

        return array(
            'data' => $this->data,
            'form'=>$form->createView()
        );
    }

    protected function getUserRedirection(User $user)
    {
        return $this->redirectToRoute("homepage",array("messages_success"=> "Welcome !"));
    }

    protected function logUserIn(User $user)
    {

        $this->get("session")->clear();
        $this->get("session")->set("rfuser", $user->getId());
    }


    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request)
    {
        $this->proceed("PUBLIC","PUBLIC");

        if ($this->getUserService()->getCurrentUser() !== null) {

            $user = $this->getUserService()->getCurrentUser();
            if (is_null($user)) {
                return $this->redirectToRoute("homepage",array("messages_error" => "Invalid Session"));
            }

            if($this->getUserService()->removeSession($user)){
                return $this->redirectToRoute("homepage",array("messages_success" => "Goodbye ".$user->getName()." !"));
            }else{
                return $this->redirectToRoute("homepage",array("messages_error" => "Error Ocurred, Please Contact Site Admin"));
            }

        }else{
            return $this->redirectToRoute("homepage",array("messages_warning" => "You need to login first"));
        }
    }

    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/forgotPassword", name="forgot_password")
     * @Template()
     */
    public function forgotPasswordAction(Request $request)
    {
        $access = $this->proceed("FORGOT PASSWORD","PUBLIC");

        if ($this->getUserService()->getCurrentUser() === null) {

            $form = $this->createFormBuilder()
                ->add('email',EmailType::class,array('label'=>'Email','required'=>true, 'attr'=>array('class'=>'form-control underlined', 'placeholder'=>'Email')))
                ->add('save', SubmitType::class, array('label' => 'Regenerate A Password','attr' => array("class"=>"btn btn-block btn-primary")))
                ->getForm();

            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();

                $userRepo = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User");
                /**
                 * @var User $user
                 */
                $user = $userRepo->findOneByEmail($data['email']);

                if (!is_null($user)) {
                    if ($user->getActive()) {

                        $password = rand(1000000,100000000);
                        $user->setPassword($password);

                        $user = $this->getUserService()->setPassword($user,$password);

                        $user->setLoginAttempt(0);

                        $this->getEntityManager()->flush();

                        $this->getEmailService()
                            ->sendMail(
                                $user->getEmail(),
                                'Password Changed For Your Account',
                                'RushFrameworkFrontOfficeBundle:Email:passwordChange.html.twig',
                                array('user'=>$user,'password'=>$password),
                                array('from_alias'=>$this->getWebsiteInfo()['name'])
                            );

                        return $this->redirectToRoute("homepage",array("messages_success" => "Verify your email for your new password"));

                    }else{
                        $form->addError(new FormError("You need to activate your account first"));
                    }
                }else{
                    $form->addError(new FormError("Sorry, there is no account with this email"));
                    $access['data']['messages']['error'] = "You can create a new account by Signing Up";
                }
            }

        }else{
            return $this->redirectToRoute("homepage",array("messages_warning" => "You are already Logged In"));
        }

        return array(
            'data' => $this->data,
            'form' => $form->createView()
        );
    }

    protected function sendActivationMail(User $user, $link, $subject = "Account Activation")
    {
        $this->getEmailService()
            ->sendMail(
                $user->getEmail(),
                $this->getWebsiteInfo()['name']." - ".$subject,
                "RushFrameworkFrontOfficeBundle:Email:activation.html.twig",
                array('link'=>$link, 'user' => $user),
                array('from_alias'=>$this->getWebsiteInfo()['name']));
    }

}