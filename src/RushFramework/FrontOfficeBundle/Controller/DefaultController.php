<?php

namespace RushFramework\FrontOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/front")
     */
    public function indexAction()
    {
        return $this->render('RushFrameworkFrontOfficeBundle:Default:index.html.twig');
    }
}
