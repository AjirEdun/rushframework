<?php

namespace RushFramework\FrontOfficeBundle\Controller;


use RushFramework\BackOfficeBundle\Controller\BackOfficeBaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EmailTestController
 * @package RushFramework\FrontOfficeBundle\Controller
 * @Route("/email/test")
 */
class EmailTestController extends BackOfficeBaseController
{
    /**
     * @param Request $request
     * @Route("/passwordChange", name="email_test_password")
     * @Template("@RushFrameworkFrontOffice/Email/passwordChange.html.twig")
     * @return array
     */
    public function passwordChangeAction(Request $request)
    {
        return array(
            'user' => $this->getUserService()->getCurrentUser(),
            'password' => 'ajirpassword'
        );
    }

    /**
     * @param Request $request
     * @Route("/sign_up", name="email_test_sign_up")
     * @Template("@RushFrameworkFrontOffice/Email/Admin/signup.html.twig")
     * @return array
     */
    public function signUpAction(Request $request)
    {
        return array(
            'user' => $this->getUserService()->getCurrentUser()
        );
    }
}