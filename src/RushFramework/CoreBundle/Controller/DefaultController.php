<?php

namespace RushFramework\CoreBundle\Controller;

use RushFramework\CoreBundle\Entity\Feature;
use RushFramework\CoreBundle\Entity\Role;
use RushFramework\CoreBundle\Entity\User;
use RushFramework\CoreBundle\Objects\BingObject;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package RushFramework\CoreBundle\Controller
 * @Route("/rush")
 */
class DefaultController extends BaseController
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('RushFrameworkCoreBundle:Default:index.html.twig');
    }

    /**
     * @Route("/init")
     */
    public function initAction()
    {
        $output = "Initializing <br/> ";

        //CREATING DEEFAULT SUPER ADMIN ROLE
        $default = new Role();
        $default->setName(\RushFramework\CoreBundle\Constants\ROLE::DEFAULT_ROLE);
        $default->setProfileSet("1");
        $this->getRoleService()->addRole($default);


        $superAdmin = new Role();
        $superAdmin->setName(\RushFramework\CoreBundle\Constants\ROLE::ADMIN_ROLE);
        $superAdmin->setProfileSet("1");

        if($this->getRoleService()->addRole($superAdmin)){
            $output .= "Role SUPER ADMIN Created Successfuly <br/> ";

            $features = array(
                array(
                    "name" => "PUBLIC",
                    "grouping" => "PUBLIC",
                    "title" => "Rush Framework"
                ),
                array(
                    "name" => "Add Feature",
                    "grouping" => "VITAL",
                    "title" => "Feature Management"
                ),
                array(
                    "name" => "Edit Feature",
                    "grouping" => "VITAL",
                    "title" => "Feature Management"
                ),
                array(
                    "name" => "View Feature",
                    "grouping" => "VITAL",
                    "title" => "Feature Management"
                ),
                array(
                    "name" => "Feature Dashboard",
                    "grouping" => "VITAL",
                    "title" => "Feature Management"
                ),
                array(
                    "name" => "ERROR PAGE",
                    "grouping" => "PUBLIC",
                    "title" => "Error Occured"
                ),
                array(
                    "name" => "SIGN UP",
                    "grouping" => "PUBLIC",
                    "title" => "Sign Up"
                ),
                array(
                    "name" => "ACTIVATION",
                    "grouping" => "PUBLIC",
                    "title" => "Activate My Account"
                ),
                array(
                    "name" => "FORGOT PASSWORD",
                    "grouping" => "PUBLIC",
                    "title" => "Fogot Password"
                ),
                array(
                    "name" => "RESEND ACTIVATION",
                    "grouping" => "PUBLIC",
                    "title" => "Resend Me My Activation Link"
                ),
                array(
                    "name" => "LOGIN",
                    "grouping" => "PUBLIC",
                    "title" => "LogIn"
                ),
                array(
                    "name" => "DASHBOARD",
                    "grouping" => "BACK OFFICE",
                    "title" => "My Back Office"
                ),
                array(
                    "name" => "DASHBOARD CORE",
                    "grouping" => "CORE MANAGEMENT",
                    "title" => "Core Management Dashboard"
                ),
                array(
                    "name" => "DASHBOARD GALLERY",
                    "grouping" => "GALLERY",
                    "title" => "My Gallery"
                ),
                array(
                    "name" => "ADD IMAGE VIDEO",
                    "grouping" => "GALLERY",
                    "title" => "Add Image or Video"
                ),
                array(
                    "name" => "DOWNLOAD GALLERY",
                    "grouping" => "PUBLIC",
                    "title" => "Download From My Gallery"
                ),
                array(
                    "name" => "DASHBOARD FILES",
                    "grouping" => "FILES",
                    "title" => "My Files"
                ),
                array(
                    "name" => "DOWNLOAD FILES",
                    "grouping" => "PUBLIC",
                    "title" => "Download My File"
                ),
                array(
                    "name" => "ADD FILE",
                    "grouping" => "FILES",
                    "title" => "Add A File In My Files"
                ),
                array(
                    "name" => "DASHBOARD ROLE",
                    "grouping" => "ROLE",
                    "title" => "Role Dashboard"
                ),
                array(
                    "name" => "ADD ROLE",
                    "grouping" => "ROLE",
                    "title" => "Add A Role"
                ),
                array(
                    "name" => "VIEW ROLE",
                    "grouping" => "ROLE",
                    "title" => "View Role"
                ),
                array(
                    "name" => "CHANGE USER ROLE",
                    "grouping" => "ROLE",
                    "title" => "Change User's Role"
                ),
                array(
                    "name" => "ACTIVATE FEATURE FOR ROLE",
                    "grouping" => "ROLE",
                    "title" => "Activate the feature for a particular role"
                ),
                array(
                    "name" => "DESACTIVATE FEATURE FOR ROLE",
                    "grouping" => "ROLE",
                    "title" => "Desactivate a feature for a role"
                ),
                array(
                    "name" => "DASHBOARD USER MANAGEMENT",
                    "grouping" => "USER MANAGEMENT",
                    "title" => "Users Dashboard"
                ),
                array(
                    "name" => "VIEW USER",
                    "grouping" => "USER MANAGEMENT",
                    "title" => "View USER"
                ),
                array(
                    "name" => "PROFILE PICTURE",
                    "grouping" => "USER",
                    "title" => "Change Profile Picture"
                ),
                array(
                    "name" => "EDIT WEBSITE CONTENT",
                    "grouping" => "CONTENT MANAGEMENT",
                    "title" => "Edit Contents In The Website"
                )
            );


            $features = $this->addFeaturesToInitMethod($features);

            $output .= " <h2>Adding Features </h2> <br/>";
            foreach ($features as $feature) {

                //Build basic features
                $publicFeature = new Feature();
                $publicFeature->setName($feature['name']);
                $publicFeature->setGrouping($feature['grouping']);
                $publicFeature->setTitle($feature['title']);
                $output .= "Feature: ".$publicFeature->getName()." <br/>";
                if ($this->getFeatureService()->addFeature($publicFeature)){
                    $output .= "Added Successfully <br/>";
                }else{
                    $output .= "Error While Adding <br/>";
                }

                if ($this->getFeatureService()->enableFeatureForRole($publicFeature,$superAdmin)){
                    $output .= "Activated for SuperAdmin <br/>";
                }else{
                    $output .= "Error Activation for SuperAdmin <br/>";
                }

            }

            $profilePictureFeature = $this->getFeatureService()->getFeatureByNameAndGrouping("PROFILE PICTURE","USER");
            if ($this->getFeatureService()->enableFeatureForRole($profilePictureFeature,$default)){
                $output .= "Activated Profile Picture for Default <br/>";
            }else{
                $output .= "Error Activation Profile Picture for Default Role <br/>";
            }

            $output .= "<h1>Creating App/data directory</h1><br/>";
            $fs = new Filesystem();
            try{
                $fs->mkdir($this->get('kernel')->getRootDir().'/data',0777);
                $fs->mkdir($this->get('kernel')->getRootDir().'/data/users',0777);
                $fs->mkdir($this->get('kernel')->getRootDir().'/data/files',0777);
                $fs->mkdir($this->get('kernel')->getRootDir().'/data/files/MyFiles',0777);
                $fs->mkdir($this->get('kernel')->getRootDir().'/data/files/MyFiles/default',0777);

                $output .= "<h2>Creating Gallery Directory</h2><br/>";
                $fs->mkdir($this->get('kernel')->getRootDir()."/../web/gallery",0777);
                $fs->mkdir($this->get('kernel')->getRootDir()."/../web/gallery/images",0777);
                $fs->mkdir($this->get('kernel')->getRootDir()."/../web/gallery/images/banners",0777);
                $fs->mkdir($this->get('kernel')->getRootDir()."/../web/gallery/videos",0777);

            }catch(\Exception $e){
                $output .= "Error Creating App/data directory <br/> Error: ".$e->getMessage() ." <br/>";
            }

            //default user account
            $email = "ajir.edun@gmail.com";
            $password = "rushadmin";
            $surname = "Edun";
            $name = "Ajir";


            $output .= "<h1>Creating User</h1><br/>";

            $user = new User();
            $user->setEmail($email);
            $user = $this->getUserService()->setPassword($user,$password);
            $user->setName($name);
            $user->setSurname($surname);
            $user->setRole($superAdmin);
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush();
            $output .= "User Created <br/>";

            $output .= "<h3>Creating User Folder</h3><br/>";
            $userId = $user->getId();
            $fs->mkdir($this->get('kernel')->getRootDir().'/data/users/'.$userId,0777);
            $fs->mkdir($this->get('kernel')->getRootDir()."/../web/data",0777);
            $fs->mkdir($this->get('kernel')->getRootDir()."/../web/data/users",0777);
            $fs->mkdir($this->get('kernel')->getRootDir()."/../web/data/users/".$userId,0777);
            $output .= "User Folder Created <br/>";

            $fileName = "profile_picture.png";
            $path = "/data/users/".$user->getId()."/";
            $fullPath = $this->get("kernel")->getRootDir(). $path;

            $fs = new Filesystem();
            $fs->copy(
                $this->get("kernel")->getRootDir().'/../web/bundles/rushframeworkbackoffice/images/profile_picture.png',
                $fullPath.$fileName
            );

            $user->setImage($path.$fileName);
            $this->getEntityManager()->flush();

            //copy image in the web folder
            $fs = new Filesystem();
            $fs->copy(
                $this->get("kernel")->getRootDir().'/../web/bundles/rushframeworkbackoffice/images/profile_picture.png',
                $this->get("kernel")->getRootDir().'/../web'.$path.$fileName
            );

            $output .= "Setting User Profile Picture Successfully <br/>";

        }else{
            $output .= "Role SUPER ADMIN ERROR <br/> ";
        }

        return new Response("<html><body>$output</body></html>");
    }

    /**
     * @Route("/admin/website" , name="rush_framework_core.website.dashboard")
     */
    public function backofficeWebsiteDashboardAction()
    {
        return new Response("<h1>Overide this route</h1>");
    }

    protected function addFeaturesToInitMethod(array $features)
    {
        return $features;
    }

    /**
     * @Route("/backgrounds", name="rush_framework_core.default.backgrounds")
     */
    public function bingAction()
    {
        $bing  = new BingObject(BingObject::YESTERDAY, 8, 'fr-FR', BingObject::RESOLUTION_LOW);

        if (!$bing) {
            return new Response("false");
        }

        if (empty($bing->getImages())) {
            return new Response("false");
        }

        return new JsonResponse(array('images'=>$bing->getImages()));
    }

    /**
     * @param Request $request
     * @param $_locale
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/language/{_locale}", name="language")
     */
    public function languageAction(Request $request, $_locale)
    {
        $url = $request->headers->get('referer');
        if ($url == "" || is_null($url)) {
            return $this->redirectToRoute('homepage');
        }

        return $this->redirect($request->headers->get('referer'));
    }
}
