<?php

namespace RushFramework\CoreBundle\Controller;


use RushFramework\CoreBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NoteController extends BaseController
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/note", name="rush_framework_core.note.find")
     */
    public function findAction(Request $request)
    {
        $this->proceed("PUBLIC","PUBLIC");

        $user = $this->getUserService()->getCurrentUser();

        if (is_null($user)){
            return new Response("Error, You should log in");
        }
        $noteList = $this->getNoteService()->getUserNoteList($user, true);

        return $this->renderTemplate('@RushFrameworkBackOffice/Utils/notes.html.twig', array(
            'data' => $this->data,
            'noteList' => $noteList
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/note/send/{id}", name="rush_framework_core.note.send")
     */
    public function sendAction(Request $request, $id)
    {
        $this->proceed("PUBLIC","PUBLIC");
        $success = false;
        if ($this->getUserService()->getCurrentUser() != null) {

            $form = $this->createFormBuilder(null,array('action' => $this->generateUrl('rush_framework_core.note.send',array('id'=>$id)),'attr'=>array('class'=>'rf-ajaxify-form')))->getForm();

            $form->add('to',HiddenType::class,array('label'=>'To', 'attr'=>array('class'=>'form-control underlined', 'placeholder'=>'Send To')));
            $form->add('content',TextareaType::class,array('label'=>'Write Your Message','required'=>true, 'attr'=>array('class'=>'form-control underlined', 'placeholder'=>'Content')));
            $form->add('submit', SubmitType::class, array('label' => 'Send Note','attr' => array("class"=>"btn btn-block btn-primary")));

            $form->get('to')->setData($id);

            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                $data = $form->getData();

                $userRepo = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User");
                /**
                 * @var User $user
                 */
                $user = $userRepo->findOneById($data['to']);

                if (!is_null($user)) {
                    $this->getNoteService()->sendUserToUser(
                        $this->getUserService()->getCurrentUser(),
                        $user,
                        $data['content']
                    );
                    $access['data']['messages']['success'] = "Note Sent :)";
                    $success = true;
                }else{
                    $form->addError(new FormError("The user you have selected is invalid"));
                    $access['data']['messages']['error'] = "The user you have selected is invalid";
                }
            }

        }else {
            return $this->redirectToRoute("homepage",array("messages_warning" => "You need to log in first"));
        }

        return $this->renderTemplate('@RushFrameworkBackOffice/Note/send.html.twig', array(
            'data' => $this->data,
            'form' => $form->createView(),
            'success' => $success
        ));
    }


    /**
     * @param Request $request
     * @return array
     * @Route("/note/mark_as_read/{id}", name="rush_framework_core.note.mark_as_read")
     */
    public function markAsReadAction(Request $request, $id)
    {
        $this->proceed("PUBLIC","PUBLIC");



        $user = $this->getUserService()->getCurrentUser();
        $noteService = $this->getNoteService();
        $noteService->markAsRead($user, $id);

        $noteList = $noteService->getUserNoteList($user, true);

        return $this->renderTemplate('@RushFrameworkBackOffice/Utils/notes.html.twig', array(
            'data' => $this->data,
            'noteList' => $noteList
        ));
    }

    /**
     * @param Request $request
     * @return array
     * @Route("/note/mark_as_unread/{id}", name="rush_framework_core.note.mark_as_unread")
     */
    public function markAsUnreadAction(Request $request, $id)
    {
        $this->proceed("PUBLIC","PUBLIC");



        $user = $this->getUserService()->getCurrentUser();
        $noteService = $this->getNoteService();
        $noteService->markAsUnread($user, $id);

        $noteList = $noteService->getUserNoteList($user, true);

        return $this->renderTemplate('@RushFrameworkBackOffice/Utils/notes.html.twig', array(
            'data' => $this->data,
            'noteList' => $noteList
        ));
    }

    /**
     * @param Request $request
     * @return array
     * @Route("/note/delete/{id}", name="rush_framework_core.note.delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->proceed("PUBLIC","PUBLIC");

        $user = $this->getUserService()->getCurrentUser();
        $noteService = $this->getNoteService();
        $noteService->delete($user, $id);

        $noteList = $noteService->getUserNoteList($user, true);

        return $this->renderTemplate('@RushFrameworkBackOffice/Utils/notes.html.twig', array(
            'data' => $this->data,
            'noteList' => $noteList
        ));
    }
}