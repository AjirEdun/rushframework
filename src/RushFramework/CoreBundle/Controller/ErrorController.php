<?php

namespace RushFramework\CoreBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ErrorController
 * @package RushFramework\CoreBundle\Controller
 * @Route("/error")
 */
class ErrorController extends BaseController
{
    /**
     * @Route("/access" , name="rush_framework_core.error.access")
     * @Template()
     */
    public function accessAction(Request $request)
    {
        $this->proceed("ERROR PAGE","PUBLIC");


        $error_message = "An error has occured, Please try again.";
        if (!is_null($request->get("error_message"))) {
            $error_message = $request->get("error_message");
        }

        if ($request->isXmlHttpRequest()) {
            return new Response("<h3>Oops!</h3><br/><p>$error_message</p>");
        }

        return array(
            'data' => $this->data,
            'message' => $error_message
        );
    }
}