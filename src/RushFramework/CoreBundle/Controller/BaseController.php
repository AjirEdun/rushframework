<?php

namespace RushFramework\CoreBundle\Controller;


use RushFramework\CoreBundle\Constants\ACCESS_METHOD;
use RushFramework\CoreBundle\Constants\LANGUAGE;
use RushFramework\CoreBundle\Exceptions\AccessException;
use RushFramework\CoreBundle\Service\AccessService;
use RushFramework\CoreBundle\Service\AccessError;
use RushFramework\CoreBundle\Service\EmailService;
use RushFramework\CoreBundle\Service\FeatureService;
use RushFramework\CoreBundle\Service\MenuService;
use RushFramework\CoreBundle\Service\NoteService;
use RushFramework\CoreBundle\Service\PaginationService;
use RushFramework\CoreBundle\Service\RoleService;
use RushFramework\CoreBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends Controller
{

    protected $data = array();

    protected $masterData = array();


    protected function proceed($featureName, $featureGrouping, $featureTitle = null)
    {
        $toReturn = $this->getAccessService()->init($featureName,$featureGrouping,$featureTitle);
        if ($toReturn['access']['right'] == true) {
            $this->masterData = $toReturn;
            $this->data = $toReturn['data'];
        }else{
            throw new AccessException($toReturn);
        }
    }

    protected function currentUserHasAccessTo($featureName)
    {
        $access = false;
        if (!empty($this->data['access']['list'])) {
            foreach ($this->data['access']['list'] as $feature) {
                if ($feature->getName() == $this->getFeatureService()->format($featureName)) {
                    return true;
                }
            }
        }

        return $access;
    }

    protected function getContents(Request $request, array &$content, $key, $name, $folder, $deletable=true)
    {
        if ($request->getLocale() == LANGUAGE::FRENCH) {
            if (file_exists($this->get('kernel')->getRootDir()."/../web/content/$folder/".$name.".html")) {
                $text = file_get_contents($this->get('kernel')->getRootDir()."/../web/content/$folder/".$name.".html");
                $content[$key]['content'] = empty($text)? null : $text;
            }else{
                $content[$key]['content'] = null;
            }
            $content[$key]['name'] = $name.'.html';
        } else {
            if (file_exists($this->get('kernel')->getRootDir()."/../web/content/$folder/".$name."-en.html")) {
                $text = file_get_contents($this->get('kernel')->getRootDir()."/../web/content/$folder/".$name."-en.html");
                $content[$key]['content'] = empty($text)? null : $text;
            }else{
                $content[$key]['content'] = null;
            }
            $content[$key]['name'] = $name.'-en.html';
        }
        $deletable == true ? $content[$key]['delete'] = true : $content[$key]['delete'] = false;
        $content[$key]['language'] = $request->getLocale();
    }

    protected function renderTemplate($view, array $params)
    {
        return $this->render($view, $params);
    }



    /**
     * @return MenuService
     */
    protected function getMenuService()
    {
        return $this->get("rush_framework_core.menu_service");
    }

    /**
     * @return RoleService
     */
    protected function getRoleService()
    {
        return $this->get("rush_framework_core.service.role");
    }

    /**
     * @return AccessService
     */
    protected function getAccessService()
    {
        return $this->get("rush_framework_core.service.access");
    }

    /**
     * @return UserService
     */
    protected function getUserService()
    {
        return $this->get("rush_framework_core.service.user");
    }

    /**
     * @return FeatureService
     */
    protected function getFeatureService()
    {
        return $this->get("rush_framework_core.service.feature");
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }


    /**
     * @return PaginationService
     */
    protected function getPaginationService()
    {
        return $this->get(PaginationService::NAME);
    }

    /**
     * @return EmailService
     */
    protected function getEmailService()
    {
        return $this->get(EmailService::NAME);
    }

    /**
     * @return NoteService
     */
    protected function getNoteService()
    {
        return $this->get(NoteService::NAME);
    }

    /**
     * @return array
     */
    protected function getWebsiteInfo()
    {
        return array(
            'name' => 'Rush Framework',
            'owner' => 'Ajir Edun'
        );
    }
}