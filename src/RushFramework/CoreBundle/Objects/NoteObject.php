<?php

namespace RushFramework\CoreBundle\Objects;


class NoteObject
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $from;

    /**
     * @var string
     */
    protected $to;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var string
     */
    protected $fromType;

    /**
     * @var string
     */
    protected $toType;

    /**
     * @var int
     */
    protected $fromId;

    /**
     * @var int
     */
    protected $toId;

    /**
     * @var bool
     */
    protected $read;

    public function __construct($id, $from, $fromId, $fromType, $to, $toId, $toType, $content, $time = null )
    {
        $this->id = $id;
        $this->from = $from;
        $this->to = $to;
        $this->content = $content;
        if ($time == null) {
            $time = new \DateTime('now');
        }
        $this->time = $time;
        $this->fromType = $fromType;
        $this->toType = $toType;
        $this->fromId = $fromId;
        $this->toId = $toId;
        $this->read = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return bool
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * @param bool $read
     */
    public function setRead($read)
    {
        $this->read = $read;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getFromType()
    {
        return $this->fromType;
    }

    /**
     * @param string $fromType
     */
    public function setFromType($fromType)
    {
        $this->fromType = $fromType;
    }

    /**
     * @return string
     */
    public function getToType()
    {
        return $this->toType;
    }

    /**
     * @param string $toType
     */
    public function setToType($toType)
    {
        $this->toType = $toType;
    }

    /**
     * @return int
     */
    public function getFromId()
    {
        return $this->fromId;
    }

    /**
     * @param int $fromId
     */
    public function setFromId($fromId)
    {
        $this->fromId = $fromId;
    }

    /**
     * @return int
     */
    public function getToId()
    {
        return $this->toId;
    }

    /**
     * @param int $toId
     */
    public function setToId($toId)
    {
        $this->toId = $toId;
    }

}