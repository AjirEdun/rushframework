<?php

namespace RushFramework\CoreBundle\Objects;


class PaginationObject
{

    const URL_KEY = "rfpage";
    const URL_NUM_KEY = 'rfnumpage';

    protected $pagination;

    public function __construct()
    {
        /*********************
         *
         * pagination : if pagination is present:true, else false
         * url_key : the key of the page to be present in the url , default = 'rfpage'
         * current : the current page number of the pagination
         * total : the total number of pages for the pagination
         * previous : the page number of the the previous page
         * next : the page number of the next page
         * variance : the variance (number of page buttons to show before current page
         * variance : 4, 5, 6 ,7 (current page), 8, 9, 10 : variance is 3
         * variance : previous : the number of page buttons to show before current page
         * variance : next : the number of page buttons to show after the current page
         *
         **********************/

        $this->pagination = array(
            'pagination' => false,
            'url_key' => '',
            'url_num_key'=> '',
            'current'=> 0,
            'total'=>0,
            'previous'=>0,
            'next'=>0,
            'variance'=>array(
                'variance'=> 0,
                'previous' => 0,
                'next' => 0,
                'previousPages' => array(),
                'nextPages' => array()
            ),
            'per_page_options' => array(
                5,10,20,50
            ),
            'data'=> array(
                'num_per_page' => null,
                'total_number' => 0,
                'offset'=>0,
            ),
        );
    }

    public function setVariancePreviousPages(array $pp)
    {
        $this->pagination['variance']['previousPages'] = $pp;
    }

    public function getVariancePreviousPages()
    {
        return $this->pagination['variance']['previousPages'];
    }

    public function setVarianceNextPages(array $pp)
    {
        $this->pagination['variance']['nextPages'] = $pp;
    }

    public function getVarianceNextPages()
    {
        return $this->pagination['variance']['nextPages'];
    }

    public function setPerPageOptions(array $pp)
    {
        $this->pagination['per_page_options'] = $pp;
    }

    public function getPerPageOptions()
    {
        return $this->pagination['per_page_options'];
    }

    public function setNumberPerPage($num){
        $this->pagination['data']['num_per_page']= $num;
    }

    public function getNumberPerPage()
    {
        return $this->pagination['data']['num_per_page'];
    }

    public function setTotalNumberOfRows($num)
    {
        $this->pagination['data']['total_number']= $num;
    }

    public function getTotalNumberOfRows()
    {
        return $this->pagination['data']['total_number'];
    }

    public function setOffset($offset)
    {
        $this->pagination['data']['offset']= $offset;
    }

    public function getOffset()
    {
        return $this->pagination['data']['offset'];
    }

    public function setIsPagination($status)
    {
        /**
         * Either true or false
         */
        $this->pagination['pagination'] = $status;
    }

    public function getIsPagination()
    {
        return $this->pagination['pagination'];
    }

    public function setUrlKey($urlKey)
    {
        $this->pagination['url_key']=$urlKey;
    }

    public function getUrlKey()
    {
        return $this->pagination['url_key'];
    }

    public function setUrlNumKey($urlKey)
    {
        $this->pagination['url_num_key']=$urlKey;
    }

    public function getUrlNumKey()
    {
        return $this->pagination['url_num_key'];
    }

    public function setCurrentPage($page)
    {
        $this->pagination['current']= $page;
    }

    public function getCurrentPage()
    {
        return $this->pagination['current'];
    }

    public function setTotalPages($page)
    {
        $this->pagination['total']= $page;
    }

    public function getTotalPages()
    {
        return $this->pagination['total'];
    }

    public function setPreviousPage($page)
    {
        $this->pagination['previous']= $page;
    }

    public function getPreviousPage()
    {
        return $this->pagination['previous'];
    }

    public function setNextPage($page)
    {
        $this->pagination['next']= $page;
    }

    public function getNextPage()
    {
        return $this->pagination['next'];
    }

    public function setVariance($variance)
    {
        //Variance is a number
        $this->pagination['variance']['variance']= $variance;

        //calculate previous variance and next variance
        $current = $this->getCurrentPage();
        $total = $this->getTotalPages();


        //PREVIOUS
        $prevDiff = $current;
        if ($prevDiff > $variance) {
            $this->pagination['variance']['previous'] = $variance - 1;
        }else{
            $this->pagination['variance']['previous'] = $prevDiff - 1;
        }

        $nextDiff = $total-$current;
        if ($nextDiff > $variance) {
            $this->pagination['variance']['next'] = $variance - 1;
        }else{
            $check = $nextDiff-1;
            if ($check<0) {
                $check = 0;
            }
            $this->pagination['variance']['next'] = $check;
        }
    }

    public function getVariance()
    {
        return $this->pagination['variance'];
    }

    /**
     * @return array
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @param array $pagination
     */
    public function setPagination($pagination)
    {
        $this->pagination = $pagination;
    }

}