<?php

namespace RushFramework\CoreBundle\Objects;


class NoteListObject
{
    const TYPE_COMPONENT = 'note_component';
    const TYPE_USER = 'user';

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $pathBackup;

    /**
     * @var array
     */
    protected $notes;

    protected $lastInsertedId;

    protected $numUnread;


    public function __construct($path, $pathBackup, array $notes = array())
    {
        $this->path = $path;
        $this->pathBackup = $pathBackup;
        $this->notes = $notes;
        $this->lastInsertedId = 0;
        $this->numUnread = 0;
    }


    public function addNote(NoteObject $note)
    {
        $this->notes[] = $note;
        $this->lastInsertedId = $note->getId();
        $this->numUnread += 1;
    }

    public function markNoteAsRead($id)
    {
        $toReturn = false;
        if (!empty($this->notes)) {
            foreach ($this->notes as $key => $note){
                if ($note['id'] == $id) {
                    if ( !$this->notes[$key]['read'] ) {
                        $this->notes[$key]['read'] = true;
                        $toReturn = true;
                    }
                }
            }
        }

        return $toReturn;
    }

    public function markNoteAsUnread($id)
    {
        $toReturn = false;
        if (!empty($this->notes)) {
            foreach ($this->notes as $key => $note){
                if ($note['id'] == $id) {
                    if ($this->notes[$key]['read']) {
                        $this->notes[$key]['read'] = false;
                        $toReturn = true;
                    }
                }
            }
        }

        return $toReturn;
    }

    public function delete($id)
    {
        $toDelete = null;

        if (!empty($this->notes)) {
            foreach ($this->notes as $key => $note){
                if ($note['id'] == $id) {
                    $toDelete = $note;
                    unset($this->notes[$key]);
                }
            }
        }

        return $toDelete;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return array
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param array $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return int
     */
    public function getLastInsertedId()
    {
        return $this->lastInsertedId;
    }

    /**
     * @param int $lastInsertedId
     */
    public function setLastInsertedId($lastInsertedId)
    {
        $this->lastInsertedId = $lastInsertedId;
    }

    /**
     * @return int
     */
    public function getNumUnread()
    {
        return $this->numUnread;
    }

    /**
     * @param int $numUnread
     */
    public function setNumUnread($numUnread)
    {
        $this->numUnread = $numUnread;
    }

    /**
     * @return string
     */
    public function getPathBackup()
    {
        return $this->pathBackup;
    }

    /**
     * @param string $pathBackup
     */
    public function setPathBackup($pathBackup)
    {
        $this->pathBackup = $pathBackup;
    }
}