<?php

namespace RushFramework\CoreBundle\Objects;


use RushFramework\CoreBundle\Exceptions\MenuException;

class MenuObject
{
    /**
     * @var String
     */
    protected $fileDir;

    /**
     * @var String
     */
    protected $fileName;

    /**
     * @var array
     */
    protected $menu;

    /**
     * Menu constructor.
     * @param String $fileDir
     * @param String $fileName
     */
    public function __construct($fileDir, $fileName)
    {
        $this->fileName = $fileName;
        $this->fileDir = $fileDir;
        $this->menu = array('ajir');
        $this->loadMenu();
    }

    /**
     * loads the menu array from the file given
     */
    protected function loadMenu()
    {
        try {
            $xml = simplexml_load_file($this->getFilePath(), null, LIBXML_NOCDATA);
        } catch (\Exception $e) {
            throw new MenuException("Error occured while opening the filed given for menu : ".$this->getFilePath());
        }

        $json = json_encode($xml);
        $this->menu = $this->transform(json_decode($json,TRUE)['menu']);

    }


    /**
     * @param array $toAdd
     * @param string $parentMenuId
     */
    public function addMenu(array $toAdd, $parentMenuId = "rush_root_dashboard")
    {
        $this->menu = $this->addInto($this->menu, $toAdd, $parentMenuId);
    }


    /**
     * @param $menu
     * @param $toAdd
     * @param $parentMenuId
     * @return array
     */
    protected function addInto($menu, $toAdd, $parentMenuId)
    {
        $newMenus = array();

        foreach ($menu as $key => $sub) {
            if ($key == $parentMenuId) {
                if (array_key_exists("menu", $sub)) {
                    $sub["menu"][$toAdd['id']] = $toAdd;
                } else {
                    $sub['menu'] = array($toAdd['id'] => $toAdd);
                }
                $menu[$key] = $sub;
            }
            if (array_key_exists("menu", $menu[$key])) {
                $menu[$key]["menu"] = $this->addInto($sub['menu'], $toAdd, $parentMenuId);
            }
            $newMenus[$key] = $menu[$key];
        }

        return $newMenus;
    }

    /**
     * RECURSIVE FUNCTION
     * @param $array
     * @return array
     */
    protected function transform($array)
    {
        $menus = array();

        if (isset($array[0])) {
            foreach ($array as $key => $menu) {
                $menus[$menu['id']] = $menu;
                if (array_key_exists('menu', $menu)) {
                    $menus[$menu['id']]['menu'] = $this->transform($menu['menu']);
                }
            }
        } else {
            $menus[$array['id']] = $array;
        }


        return $menus;
    }

    protected function sortByOrder()
    {
        //@todo sort by order
    }

    /**
     * @return string
     */
    protected function getFilePath()
    {
        return str_replace('rf-','',$this->getFileDir()). "/" . $this->fileName;
    }

    /**
     * @return array
     */
    public function getMenu()
    {
        if (!is_null($this->menu)) {
            $this->sortByOrder();
        }

        return $this->menu;
    }

    /**
     * @param array $menu
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return String
     */
    public function getFileDir()
    {
        return $this->fileDir;
    }

    /**
     * @param String $fileDir
     */
    public function setFileDir($fileDir)
    {
        $this->fileDir = $fileDir;
    }

    /**
     * @return String
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param String $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

}