<?php

namespace RushFramework\CoreBundle\Objects;


use Symfony\Component\DependencyInjection\ContainerInterface;

class AbstractContainerAwareObject
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * AbstractContainerAwareObject constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return ContainerInterface
     * @throws \Exception
     */
    public function getContainer()
    {

        if (is_null($this->container)) {
            throw new \Exception("You have not set the container :p");
        }

        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     * @return $this
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;

        return $this;
    }
}