<?php

namespace RushFramework\CoreBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class AppExtension extends \Twig_Extension
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return "core.twig_extension";
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('hasAccess', array($this, 'checkAccessFunction'))
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('timeSince', array($this, 'getTimeFrom'))
        );
    }

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function checkAccessFunction($featureName, $accessList)
    {

        $access= false;

        foreach ($accessList as $feature) {
            if ($feature->getName() == $featureName) {
                return true;
            }
        }

        return $access;

    }

    public function getTimeFrom($time)
    {
        return $this->timeAgo($time);

    }

    function timeAgo($timestamp){
        $datetime1=new \DateTime("now");
        $datetime2=date_create($timestamp);
        $diff=date_diff($datetime1, $datetime2);
        $timemsg='';
        if($diff->y > 0){
            $timemsg = $diff->y .' yr'. ($diff->y > 1?"s":'');

        }
        else if($diff->m > 0){
            $timemsg = $diff->m . ' month'. ($diff->m > 1?"s":'');
        }
        else if($diff->d > 0){
            $timemsg = $diff->d .' day'. ($diff->d > 1?"s":'');
        }
        else if($diff->h > 0){
            $timemsg = $diff->h .' hr'.($diff->h > 1 ? "s":'');
        }
        else if($diff->i > 0){
            $timemsg = $diff->i .' min'. ($diff->i > 1?"s":'');
        }
        else if($diff->s > 0){
            $timemsg = $diff->s .' sec'. ($diff->s > 1?"s":'');
        }

        //$timemsg = $timemsg.' ago';
        return $timemsg;
    }

}