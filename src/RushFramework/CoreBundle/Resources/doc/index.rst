RushFramework / CoreBundle
==========================

This bundle provides the core functionality of RushFramework.

What does it do?
----------------

* User Management
* Role Management
* Authentification & Authorisation Management (Login, Sign-Up, Forgotten Password)
* Helper Functions likes :
    - User Has Access
    - Bundle Exists
* Profile Set


