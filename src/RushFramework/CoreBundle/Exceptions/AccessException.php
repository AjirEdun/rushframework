<?php

namespace RushFramework\CoreBundle\Exceptions;


use Exception;

class AccessException extends \Exception
{

    /**
     * @var String
     */
    protected $method;

    /**
     * @var String
     */
    protected $url;

    /**
     * AccessException constructor.
     * @param array $data
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(array $data, $code = 0, Exception $previous = null)
    {

        $this->method = $data['access']['method'];
        if (isset($data['access']['url'])) {
            $this->url = $data['access']['url'];
        }
        parent::__construct($data['access']['message'], $code, $previous);
    }

    /**
     * @return String
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param String $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return String
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param String $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


}