<?php

namespace RushFramework\CoreBundle\EventSubscriber;

use RushFramework\CoreBundle\Constants\ACCESS_METHOD;
use RushFramework\CoreBundle\Exceptions\AccessException;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class AccessSubscriber implements EventSubscriberInterface
{

    /**
     * @var Router
     */
    protected $router;

    /**
     * AccessSubscriber constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {

        $exception = $event->getException();

        if ($exception instanceof AccessException) {


            if ($exception->getMethod() == ACCESS_METHOD::LOGIN && !is_null($exception->getUrl())) {

                $event->setResponse(
                    new RedirectResponse(
                        $this->router->generate(
                            'login', array(
                                'rush-url'=>$exception->getUrl(),
                                'messages_error'=>$exception->getMessage()
                            )
                        )
                    )
                );
            } else {
                $event->setResponse(
                    new RedirectResponse(
                        $this->router->generate('rush_framework_core.error.access',
                            array('error_message'=>$exception->getMessage())
                        )
                    )
                );
            }
        }

    }


    public static function getSubscribedEvents()
    {
        return array(
            'kernel.exception' => 'onKernelException'
        );
    }

}