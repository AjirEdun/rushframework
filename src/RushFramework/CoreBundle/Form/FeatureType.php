<?php

namespace RushFramework\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeatureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label'=>'Feature Name*',
                'required'=>true,
                'attr' => array(
                    'data-toggle'=>'helper',
                    'title'=> 'The name of the feature'
                )
            ))
            ->add('title', TextType::class, array(
                'label'=>'Feature Title*',
                'required'=>true,
                'attr' => array(
                    'data-toggle'=>'helper',
                    'title'=> 'The name to appear on the tab of the browser'
                )
            ))
            ->add('grouping', TextType::class, array(
                'label'=>'Feature Group*',
                'required'=>true,
                'attr' => array(
                    'data-toggle'=>'helper',
                    'title'=> 'The group of the feature, it is use for Grouping features together'
                )
            ))
            ->add('enable', CheckboxType::class, array(
                'label'=>'Enable Feature?',
                'required'=> false
            ))
        ;
    }




    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'=>'RushFramework\CoreBundle\Entity\Feature'
            )
        );
    }
}