<?php

namespace RushFramework\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserSignUpType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', FileType::class, array(
                'label'=>'Profile Picture',
                'data_class'=> null,
                'required'=>false,
                'attr' => array(
                    'data-toggle'=>"tooltip",
                    'title'=>"The image size should be less than 2Mb",
                    'class' => 'form-control underlined'
                )
            ))
            ->add('name', TextType::class, array(
                'label'=>'Name',
                'required'=>true,
                'attr' => array(
                    'class' => 'form-control underlined'
                )
            ))
            ->add('surname', TextType::class, array(
                'label'=>'Surname',
                'required'=>true,
                'attr' => array(
                    'class' => 'form-control underlined'
                )
            ))
            ->add('email', EmailType::class, array(
                'label'=>'Email',
                'required'=>true,
                'attr' => array(
                    'class' => 'form-control underlined'
                )
            ))
            ->add('password', PasswordType::class, array(
                'label'=>'Password',
                'required'=> true,
                'attr' => array(
                    'class' => 'form-control underlined'
                )
            ))
            ->add('confirmPassword', PasswordType::class, array(
                'label'=>'Confirm Password',
                'required'=> true,
                'mapped'=>false,
                'attr' => array(
                    'class' => 'form-control underlined'
                )
            ))
        ;
    }




    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'=>'RushFramework\CoreBundle\Entity\User'
            )
        );
    }
}