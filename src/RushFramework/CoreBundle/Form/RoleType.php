<?php

namespace RushFramework\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, array(
                'label'=>'Role Name*',
                'required'=>true
            ))
            ->add("profileSet", IntegerType::class, array(
                'label' => "Profile Set*",
                'required'=>true,
                'disabled'=>true,
                'attr' => array(
                    'data-toggle'=>'helper',
                    'title'=>'Use to determine the data the roles can have'
                )
            ))
        ;
    }




    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'=>'RushFramework\CoreBundle\Entity\Role'
            )
        );
    }
}