<?php

namespace RushFramework\CoreBundle\Constants;


class FEATURE
{
    const PUBLIC_FEATURE= "PUBLIC";
    const VITAL_FEATURE = "VITAL";
}