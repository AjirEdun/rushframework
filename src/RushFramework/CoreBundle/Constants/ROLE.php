<?php

namespace RushFramework\CoreBundle\Constants;

/**
 * Class ROLE
 * @package RushFramework\CoreBundle\Constants
 */
class ROLE
{
    /**
     * DEFINING ALL BASIC CONSTANTS FOR ROLE
     */

    const DEFAULT_ROLE = "DEFAULT";
    const ADMIN_ROLE = "ADMIN";

}