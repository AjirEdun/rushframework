<?php

namespace RushFramework\CoreBundle\Constants;


class ACCESS_METHOD
{
    const NONE = "none";
    const LOGIN = "login";
    const REDIRECT = "redirect";
}