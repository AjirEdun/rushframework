<?php

namespace RushFramework\CoreBundle\Constants;


class NOTE_COMPONENT
{
    const ROLE_MANAGER = 'Role Manager';
    const FEATURE_MANAGER = 'feature_manager';
    const USER_MANAGER = 'user_manager';
}