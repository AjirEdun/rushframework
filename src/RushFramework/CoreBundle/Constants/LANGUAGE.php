<?php

namespace RushFramework\CoreBundle\Constants;


class LANGUAGE
{
    const ENGLISH = 'en';
    const FRENCH = 'fr';
}