<?php

namespace RushFramework\CoreBundle\Event;


use RushFramework\CoreBundle\Exceptions\MenuException;
use RushFramework\CoreBundle\Objects\MenuObject;

class MenuEvent extends AbstractEvent
{

    const LOAD = "rush_framework_core.menu";

    /**
     * @var String
     */
    protected $fileDirectory;

    /**
     * @var String
     */
    protected $fileName;

    /**
     * @var MenuObject
     */
    protected $menuObject;

    /**
     * @var String
     */
    protected $name;

    public function load(array $config)
    {
        if (array_key_exists('file_name', $config)) {
            $this->fileName = $config['file_name'];
        } else {
            throw new MenuException("Menu: ".$this->getName()." - You haven't define the file_name parameter for your menu");
        }

        if (array_key_exists('file_directory', $config)) {
            $this->fileDirectory = $config['file_directory'];
        } else {
            throw new MenuException("Menu: ".$this->getName()." - You haven't define the file_directory parameter for your menu");
        }

        $this->setMenuObject(new MenuObject($this->fileDirectory, $this->fileName));
    }

    /**
     * @return array
     * @throws MenuException
     */
    public function getMenu()
    {
        if ($this->menuObject !== null) {
            return $this->menuObject->getMenu();
        } else {
            throw new MenuException("Menu: ".$this->getName()." - Cannot access menu object");
        }
    }

    /**
     * @return MenuObject
     */
    public function getMenuObject()
    {
        return $this->menuObject;
    }

    /**
     * @param MenuObject $menuObject
     */
    public function setMenuObject($menuObject)
    {
        $this->menuObject = $menuObject;
    }

    /**
     * @return String
     */
    public function getFileDirectory()
    {
        return $this->fileDirectory;
    }

    /**
     * @param String $fileDirectory
     */
    protected function setFileDirectory($fileDirectory)
    {
        $this->fileDirectory = $fileDirectory;
    }

    /**
     * @return String
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param String $fileName
     */
    protected function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param String $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}