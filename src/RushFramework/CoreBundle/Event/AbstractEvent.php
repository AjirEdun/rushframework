<?php

namespace RushFramework\CoreBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class AbstractEvent extends Event
{
    /**
     * @todo "if there are any common things in all the events"
     */
}