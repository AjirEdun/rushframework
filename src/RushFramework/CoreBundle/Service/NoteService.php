<?php

namespace RushFramework\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use RushFramework\CoreBundle\Entity\User;
use RushFramework\CoreBundle\Objects\NoteListObject;
use RushFramework\CoreBundle\Objects\NoteObject;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class NoteService
{

    const NUM_READ_NOTES = 3;

    const NAME = 'rush_framework_core.service.note';

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var array
     */
    protected $components;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var Kernel
     */
    protected $kernel;

    public function __construct(EntityManager $entityManager, Serializer $serializer, Kernel $kernel, array $components)
    {
        $this->entityManager = $entityManager;
        $this->components = $components;
        $this->serializer = $serializer;
        $this->kernel = $kernel;
    }

    public function sendUserToUser(User $from, User $to, $content, $sendEmail = false )
    {
        // get from and to note list
        $toNoteList = $this->getUserNoteList($to);
        $toNoteListBackup = $this->getUserNoteListBackup($toNoteList);

        $note = new NoteObject(
            $toNoteList->getLastInsertedId()+1,
            $from->getName(). " " . $from->getSurname(),
            $from->getId(),
            NoteListObject::TYPE_USER,
            $to->getName() . " " . $to->getSurname(),
            $to->getId(),
            NoteListObject::TYPE_USER,
            $content
        );

        $toNoteList = $this->cleanNoteList($toNoteList);

        $toNoteList->addNote($note);
        $toNoteListBackup->addNote($note);

        $this->setUserNoteLists($toNoteList,$toNoteListBackup);

    }

    public function sendComponentToUser($from, User $to, $content, $sendEmail = false)
    {

        /**
         * @var NoteListObject $toNoteList
         * @var NoteListObject $toNoteListBackup
         */

        $toNoteList = $this->getUserNoteList($to);
        $toNoteListBackup = $this->getUserNoteListBackup($toNoteList);

        $note = new NoteObject(
            $toNoteList->getLastInsertedId()+1,
            $from,
            -1,
            NoteListObject::TYPE_COMPONENT,
            $to->getName() . " " . $to->getSurname(),
            $to->getId(),
            NoteListObject::TYPE_USER,
            $content
        );

        $toNoteList = $this->cleanNoteList($toNoteList);

        $toNoteList->addNote($note);
        $toNoteListBackup->addNote($note);

        $this->setUserNoteLists($toNoteList,$toNoteListBackup);

    }

    public function sendUserToComponent(User $from, $to, $content)
    {
        //can be use for AI Purposes, generating automated tasks via messaging
    }

    public function sendComponentToComponent($from, $to, $content)
    {
        //can be use for AI Purposes, generating automated tasks via messaging
    }

    protected function cleanNoteList(NoteListObject $noteList)
    {

        $notes = $noteList->getNotes();
        $numUnread = $noteList->getNumUnread();
        if ((count($notes) - $numUnread) > NoteService::NUM_READ_NOTES) {
            $numToDelete = (count($notes) - $numUnread) - NoteService::NUM_READ_NOTES;

            foreach ($notes as $key => $note){
                if ($note['read']) {
                    if ($numToDelete > 0) {
                        unset($notes[$key]);
                        $numToDelete -= 1;
                    }
                }
            }
            $noteList->setNotes($notes);
        }


        return $noteList;
    }

    public function markAsRead(User $user, $noteId)
    {

        /**
         * @var NoteListObject $noteList $noteListBackup
         */
        $noteList = $this->getUserNoteList($user);

        $ret = $noteList->markNoteAsRead($noteId);

        if ($ret) {
            $noteList->setNumUnread($noteList->getNumUnread() -1);
        }

        $this->setUserNoteLists($noteList);
    }


    public function markAsUnread(User $user, $noteId)
    {

        /**
         * @var NoteListObject $noteList $noteListBackup
         */


        $noteList = $this->getUserNoteList($user);

        $ret = $noteList->markNoteAsUnread($noteId);

        if ($ret) {
            $noteList->setNumUnread($noteList->getNumUnread() +1);
        }

        $this->setUserNoteLists($noteList);
    }

    public function delete(User $user, $noteId)
    {

        /**
         * @var NoteListObject $noteList $noteListBackup
         */


        $noteList = $this->getUserNoteList($user);

        $note = $noteList->delete($noteId);

        if ( !is_null($note) && !$note['read']) {
            $noteList->setNumUnread($noteList->getNumUnread() - 1);
        }

        $this->setUserNoteLists($noteList);
    }


    public function setUserNoteLists(NoteListObject $noteList, NoteListObject $noteListBackup = null)
    {
        $path = $this->kernel->getRootDir().$noteList->getPath();
        $pathBackup = $this->kernel->getRootDir().$noteList->getPathBackup();

        $notesListJson = $this->serializer->serialize($noteList, 'json');
        file_put_contents($path, $notesListJson);

        if (!is_null($noteListBackup)) {
            $notesListBackupJson = $this->serializer->serialize($noteListBackup, 'json');
            file_put_contents($pathBackup, $notesListBackupJson);
        }

    }

    public function getUserNoteListBackup(NoteListObject $noteList)
    {
        $path = $this->kernel->getRootDir().$noteList->getPathBackup();

        $notesListBackupJson = file_get_contents($path);
        $notesListBackup = $this->serializer->deserialize($notesListBackupJson, NoteListObject::class,'json');

        return $notesListBackup;

    }

    public function getUserNoteList(User $user, $prepareForDisplay = false)
    {
        $path = "/data/users/".$user->getId()."/". "notesList.json";
        $notesList = $this->kernel->getRootDir(). $path;

        $pathBackup = "/data/users/".$user->getId()."/". "notesListBackup.json";
        $notesListBackup = $this->kernel->getRootDir(). $pathBackup;

        if(file_exists($notesList)){
            $notesListJson = file_get_contents($notesList);
        } else {
            $notesListObject = new NoteListObject($path, $pathBackup, array());
            $notesListJson = $this->serializer->serialize($notesListObject, 'json');
            file_put_contents($notesList, $notesListJson);
        }

        try {
            $notesList = $this->serializer->deserialize($notesListJson, NoteListObject::class,'json');
        } catch(\Exception $e){
            $notesListObject = new NoteListObject($pathBackup,array());
            $notesListJson = $this->serializer->serialize($notesListObject, 'json');
            file_put_contents($notesList, $notesListJson);
            $notesList = $this->serializer->deserialize($notesListJson, NoteListObject::class,'json');
        }

        if (file_exists($notesListBackup)) {
            $notesListBackupJson = file_get_contents($notesListBackup);
        } else {
            $notesListBackupObject = new NoteListObject($path, $pathBackup, array());
            $notesListBackupJson = $this->serializer->serialize($notesListBackupObject, 'json');
            file_put_contents($notesListBackup, $notesListBackupJson);
        }


        if ($prepareForDisplay){
            $notesList = $this->prepareToDisplay($notesList);
        }

        return $notesList;

    }

    public function prepareToDisplay(NoteListObject $noteList)
    {

        $notes = $noteList->getNotes();

        $notesInversed = array_reverse($notes, true);

        $noteList->setNotes($notesInversed);

        return $noteList;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    protected function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getComponents()
    {
        return $this->components;
    }

    /**
     * @param array $components
     */
    protected function setComponents($components)
    {
        $this->components = $components;
    }
}