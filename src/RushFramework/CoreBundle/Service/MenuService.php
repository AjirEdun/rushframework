<?php

namespace RushFramework\CoreBundle\Service;

use RushFramework\CoreBundle\Event\MenuEvent;
use RushFramework\CoreBundle\Exceptions\MenuException;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Kernel;

class MenuService
{
    /**
     * @var Kernel
     */
    protected $kernel;

    /**
     * @var array
     */
    protected $menuParams;

    /**
     * @var EventDispatcher
     */
    protected $eventDispatcher;

    /**
     * MenuService constructor.
     * @param Kernel $kernel
     * @param array $menuParams
     */
    public function __construct(Kernel $kernel,EventDispatcherInterface  $eventDispatcher, array $menuParams)
    {
        $this->kernel = $kernel;
        $this->menuParams = $menuParams;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param $id
     * @return array
     */
    public function getMenu($id)
    {
        $menuParams = $this->getMenuParametersById($id);

        $menuEvent = new MenuEvent();
        $menuEvent->setName($id);
        $menuEvent->load($menuParams);

        $this->eventDispatcher->dispatch(MenuEvent::LOAD, $menuEvent);

        return $menuEvent->getMenu();
    }

    /**
     * @param $id
     * @return mixed
     * @throws MenuException
     */
    protected function getMenuParametersById($id)
    {
        if (array_key_exists($id,$this->menuParams)) {

            return $this->processFileDirectory($this->menuParams[$id]);
        } else {
            throw new MenuException("It seems that you haven't added your menu parameters in the parameter list.");
        }
    }

    protected function processFileDirectory(array $menuParams)
    {
        if (array_key_exists('file_directory', $menuParams)) {
            $menuParams['file_directory'] =
                $this->kernel->locateResource(str_replace('rf-','',$menuParams['file_directory']));
        }

        return $menuParams;
    }

}