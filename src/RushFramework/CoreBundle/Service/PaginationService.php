<?php

namespace RushFramework\CoreBundle\Service;


use RushFramework\CoreBundle\Objects\PaginationObject as Pagination;
use Doctrine\ORM\EntityManager;

class PaginationService
{

    const NAME = "rush_framework_core.service.pagination";

    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }



    public function init(
        $totalNumberOfRows,
        $currentPage = 1,
        $numberPerPage = 10,
        $variance = 2,
        $url_key= Pagination::URL_KEY,
        $url_num_key = Pagination::URL_NUM_KEY,
        $numPerPageOptions = array())
    {

        if ($currentPage == null) {
            $currentPage = 1;
        }

        if ($numberPerPage == null){
            $numberPerPage = 10;
        }

        $pagination = new Pagination();

        if ($numberPerPage>=$totalNumberOfRows) {
            $numberPerPage = $totalNumberOfRows;
        }

        $totalNumberOfPages = ceil($totalNumberOfRows/$numberPerPage);

        if ($totalNumberOfPages == 1) {
            $pagination->setIsPagination(false);
            return new Pagination();
        }else{
            $pagination->setIsPagination(true);
        }

        if ($currentPage >= $totalNumberOfPages) {
            $currentPage = $totalNumberOfPages;
        }

        $pagination->setCurrentPage($currentPage);
        $pagination->setNumberPerPage($numberPerPage);
        $pagination->setTotalNumberOfRows($totalNumberOfRows);

        if ($currentPage != 1) {
            $pagination->setPreviousPage($currentPage-1);
        }
        $pagination->setOffset($numberPerPage*$pagination->getPreviousPage());

        $pagination->setTotalPages($totalNumberOfPages);


        if ($totalNumberOfPages - $currentPage > 0)
        {
            $pagination->setNextPage($currentPage+1);
        }

        $pagination->setVariance($variance);

        $pagination->setUrlKey($url_key);
        $pagination->setUrlNumKey($url_num_key);

        $variancePrevious = $pagination->getVariance()['previous'];
        $previousArray = array();
        for ($i = $variancePrevious+1; $i > 0; --$i) {
            $p = $currentPage - $i;
            if ($p>1) {
                $previousArray[] = $p;
            }
        }

        $varianceNext = $pagination->getVariance()['next'];
        $nextArray = array();
        for ($i = 0; $i<$varianceNext+1;$i++){
            $p = $currentPage + $i +1;
            if ($p < $totalNumberOfPages) {
                $nextArray[] = $p;
            }
        }
        $pagination->setVariancePreviousPages($previousArray);
        $pagination->setVarianceNextPages($nextArray);

        if (!empty($numPerPageOptions)) {
            $pagination->setPerPageOptions($numPerPageOptions);
        }


        return $pagination;
    }

}