<?php

namespace RushFramework\CoreBundle\Service;


use Doctrine\ORM\EntityManager;
use RushFramework\CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;

class UserService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Session
     */
    private $session;

    /**
     * UserService constructor.
     * @param EntityManager $entityManager
     * @param Session $session
     */
    public function __construct(EntityManager $entityManager, Session $session)
    {
        $this->em = $entityManager;
        $this->session= $session;
    }

    /**
     * Generating random sugar for user creation
     * @return string
     */
    public function generateSugar()
    {
        return md5(rand(1,10000));
    }

    /**
     * @param $id
     * @return null|object|User
     */
    public function getUser($id)
    {
        $user = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User")->find($id);
        if (is_null($user)){
            return null;
        }

        return $user;
    }


    public function getCurrentUser()
    {
        if ($this->getSession()->has("rfuser")) {
            if ($user_id = intval($this->getSession()->get("rfuser"))) {
                $user = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User")->find($user_id);

                return $user;
            }
        }

        return null;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function removeSession(User $user)
    {
        $user = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User")->find($user->getId());
        if (is_null($user)) {
            return false;
        }

        $user->setOnline(false);
        $this->getEntityManager()->flush($user);

        $this->getSession()->remove("rfuser");

        return true;
    }

    /**
     * @param User $user
     * @param $password
     * @return User
     */
    public function setPassword(User $user,$password) {
        if (!$user->getSugar()) {
            $user->setSugar(md5(rand(1,10000)));
        }
        $user->setPassword($this->formatPassword($user,$password));
        return $user;
    }

    /**
     * @param User $user
     * @param null $password
     * @return string
     */
    public function formatPassword(User $user,$password = null)
    {
        $pass = null;
        if ($password) {
            $pass = md5($password);
        }else{
            $pass = md5($user->getPassword());
        }

        return ($pass . $user->getSugar());
    }

    /**
     *
     */
    public function refreshUserOnline()
    {
        $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:User")->refreshOnline();
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->em;
    }

    /**
     * @return Session
     */
    protected function getSession()
    {
        return $this->session;
    }


}