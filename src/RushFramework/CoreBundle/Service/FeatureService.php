<?php

namespace RushFramework\CoreBundle\Service;

use RushFramework\CoreBundle\Entity\Feature;
use RushFramework\CoreBundle\Entity\Role;
use Doctrine\ORM\EntityManager;

class FeatureService
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * FeatureService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param $name
     * @param $grouping
     * @return mixed
     */
    public function getFeatureByNameAndGrouping($name,$grouping){
        $name = $this->format($name);
        $grouping = $this->format($grouping);
        $result = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")
            ->getFeatureByNameAndGroupingQueryBuilder($name,$grouping)
            ->getQuery()->getOneOrNullResult();

        return $result;
    }

    /**
     * @return array
     */
    public function getFeatureGroupings()
    {
        $arrays = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->getFeaturesGroups();

        $groups = array();
        foreach ($arrays as $group) {
            $groups[] = $group[1];
        }

        return $groups;
    }

    /**
     * @param Feature $feature
     * @return bool
     */
    public function isFeaturePublic(Feature $feature)
    {
        if ($feature->getGrouping() == \RushFramework\CoreBundle\Constants\FEATURE::PUBLIC_FEATURE ) {
            return true;
        }
        return false;
    }

    /**
     * @return array|Feature[]
     */
    public function getAllFeatures()
    {
        return $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->findAll();
    }

    /**
     * @param Feature $feature
     * @return Feature
     */
    public function formatFeature(Feature $feature)
    {
        $feature->setName($this->format($feature->getName()));
        $feature->setGrouping($this->format($feature->getGrouping()));

        return $feature;
    }

    /**
     * @param $grouping
     * @return bool
     */
    public function isGroupingExist($grouping)
    {
        return $this->getEntityManager()
                ->getRepository("RushFrameworkCoreBundle:Feature")
                    ->isGroupingExist($this->format($grouping));
    }

    /**
     * @param $name
     * @param $grouping
     * @return bool
     */
    public function isNameInGroupingExist($name,$grouping)
    {
        return $this->getEntityManager()
                ->getRepository("RushFrameworkCoreBundle:Feature")
                    ->isFeatureNameExistInGroup($this->format($name),$this->format($grouping));
    }

    /**
     * @param Feature $feature
     * @param Role $role
     * @return bool
     */
    public function enableFeatureForRole(Feature $feature, Role $role)
    {
        if ($feature = $this->getFeatureById($feature->getId())) {
            $connection = $this->getEntityManager()->getConnection();
            $sql = 'UPDATE `featureaccess` SET `f' . $feature->getId() . '` = 1 WHERE role_id = '.$role->getId().' ;';
            $connection->exec($sql);
            return true;
        }
        return false;
    }

    /**
     * @param Feature $feature
     * @param Role $role
     * @return bool
     */
    public function disableFeatureForRole(Feature $feature, Role $role)
    {
        if ($role->getName() !== \RushFramework\CoreBundle\Constants\ROLE::ADMIN_ROLE) {
            if($feature = $this->getFeatureById($feature->getId())) {
                $connection = $this->getEntityManager()->getConnection();
                $sql = 'UPDATE `featureaccess` SET `f' . $feature->getId() . '` = 0 WHERE role_id = '.$role->getId().' ;';
                $connection->exec($sql);
                return true;
            }
        }
        return false;
    }

    /**
     * @param Feature $feature
     * @return bool
     */
    public function activateFeature(Feature $feature)
    {
        if ($feature->getEnable()) {
            return true;
        }else{
            if ($feature = $this->getFeatureById($feature->getId())) {
                $feature->setEnable(true);
                $this->getEntityManager()->flush($feature);
                return true;
            }
        }
        return false;
    }

    /**
     * @param Feature $feature
     * @return bool
     */
    public function disactivateFeature(Feature $feature)
    {
        if (!$feature->getEnable()) {
            return true;
        }
        if ($feature = $this->getFeatureById($feature->getId())) {
            if ($feature->getGrouping() !== \RushFramework\CoreBundle\Constants\FEATURE::VITAL_FEATURE) {
                $feature->setEnable(false);
                $this->getEntityManager()->flush($feature);
                return true;
            }
        }
        return false;
    }

    /**
     * @param Feature $feature
     * @return bool
     */
    public function addFeature(Feature $feature)
    {
        $feature = $this->formatFeature($feature);

        if (!$feature->getId() &&
            count($this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->findByName($feature->getName())) == 0) {
            //add feature
            $this->getEntityManager()->persist($feature);
            $this->getEntityManager()->flush();

            //Add in featureaccess table
            $this->createFeatureAccess($feature);

            return true;
        }
        return false;
    }

    /**
     * @param Feature $feature
     * @return bool
     */
    public function removeFeature(Feature $feature)
    {

        if ($feature = $this->getFeatureById($feature->getId())) {
            $this->deleteFeatureAccess($feature);

            $this->getEntityManager()->remove($feature);
            $this->getEntityManager()->flush();
            return true;
        }
        return false;
    }


    /**
     * @param Feature $feature
     */
    protected function createFeatureAccess(Feature $feature)
    {
        $connection = $this->getEntityManager()->getConnection();
        $zero = 0;
        $one = 1;
        if ($feature->getGrouping() == \RushFramework\CoreBundle\Constants\FEATURE::PUBLIC_FEATURE) {
            $sql = "ALTER TABLE `featureaccess` ADD `f".$feature->getId()."` TINYINT NOT NULL DEFAULT ".$one." ;";
        }else{
            $sql = "ALTER TABLE `featureaccess` ADD `f".$feature->getId()."` TINYINT NOT NULL DEFAULT ".$zero." ;";
        }
        $connection->exec($sql);
    }

    /**
     * @return array
     */
    public function getAllFeaturesByGrouping()
    {
        $features = $this->getAllFeatures();
        $grouped = array();


        if (count($features)>0) {
            foreach ($features as $feature) {
                /**
                 * @var Feature $feature
                 */
                if (array_key_exists($feature->getGrouping(),$grouped)) {
                    $array = array(
                        'feature' => $feature
                    );
                    $grouped[$feature->getGrouping()][] = $array;
                }else{
                    $grouped[$feature->getGrouping()] = array();
                    $array = array(
                        'feature' => $feature
                    );
                    $grouped[$feature->getGrouping()][] = $array;
                }
            }
        }


        return $grouped;
    }


    /**
     * @param Feature $feature
     */
    protected function deleteFeatureAccess(Feature $feature)
    {
        $connection = $this->getEntityManager()->getConnection();
        $sql = 'ALTER TABLE `featureaccess` DROP column `f' . $feature->getId() . '` ;';
        $connection->exec($sql);
    }

    /**
     * @param $toFormat
     * @return string
     */
    public function format($toFormat)
    {
        return strtoupper(trim($toFormat));
    }

    /**
     * @param $id
     * @return bool|null|object|Feature
     */
    protected  function getFeatureById($id){
        if (intval($id)) {
            $feature = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->find($id);
            if ($feature instanceof Feature) {
                return $feature;
            }
        }
        return false;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->em;
    }
}