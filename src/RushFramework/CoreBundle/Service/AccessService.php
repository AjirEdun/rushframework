<?php

namespace RushFramework\CoreBundle\Service;



use RushFramework\CoreBundle\Constants\ACCESS_METHOD;
use RushFramework\CoreBundle\Entity\Feature;
use RushFramework\CoreBundle\Entity\User;
use RushFramework\CoreBundle\Service\AbstractContainerAwareService;
use RushFramework\CoreBundle\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AccessService extends AbstractContainerAwareService
{
    /**************************************************
     *
     *   This is a service that needs to be called in every controller.
     *   It initializes all the basic data.
     *
     **************************************************/


    protected $userService;


    public function __construct(ContainerInterface $containerInterface)
    {
        parent::__construct($containerInterface);
        $this->userService = $this->getContainer()->get("rush_framework_core.service.user");
    }

    public function init($featureName, $featureGrouping, $featureTitle = null)
    {
        //initializing
        $toReturn = array();
        $toReturn['data'] = array();
        $toReturn['access']['right'] = false;
        $toReturn['access']['method'] = ACCESS_METHOD::NONE;
        $toReturn['data']['date'] = new \DateTime("now");
        $toReturn['access']['message'] = "Oops, Please Contact The Site Admin";
        $toReturn['data']['language'] = $this->getRequest()->getLocale();

        $user = new User();
        $user->setName("Login");
        $user->setImage("/bundles/rushframeworkbackoffice/images/profile_picture.png");

        $toReturn['data']['note'] = null;
        $toReturn['data']['user']= $user;
        $toReturn['data']['title'] = $featureTitle;
        $toReturn['data']['access']['list'] = array();

        //if request has get messages
        $toReturn['data']['messages']["success"] = null;
        $toReturn['data']['messages']["info"] = null;
        $toReturn['data']['messages']["warning"] = null;
        $toReturn['data']['messages']["error"] = null;

        //Initializing messages
        if (!is_null($this->getRequest()->get("messages_success"))) {
            $toReturn['data']['messages']["success"] = $this->getRequest()->get("messages_success");
        }
        if (!is_null($this->getRequest()->get("messages_info"))) {
            $toReturn['data']['messages']["info"] = $this->getRequest()->get("messages_info");
        }
        if (!is_null($this->getRequest()->get("messages_warning"))) {
            $toReturn['data']['messages']["warning"] = $this->getRequest()->get("messages_warning");
        }
        if (!is_null($this->getRequest()->get("messages_error"))) {
            $toReturn['data']['messages']["error"] = $this->getRequest()->get("messages_error");
        }

        //if this request contain a rush-url parameter
        if (!is_null($this->getRequest()->get("rush-url"))) {
            $toReturn['access']['method'] = ACCESS_METHOD::REDIRECT;
            $toReturn['access']['url'] = $this->getRequest()->get('rush-url');
        }


        /**
         * @var User $user
         */
        if ($user = $this->getUserService()->getCurrentUser()) {
            $user->setOnline(true);
            $user->setLastLogin(new \DateTime('now'));
            $this->getEntityManager()->flush();
            $toReturn['data']['user'] = $user;

            //get user notes
            //$noteList = $this->getContainer()->get('rush_framework_core.service.note')->getUserNoteList($user);
            //$toReturn['data']['note']  = $noteList;
        }else{
            // No Session
            // Log into the database
            // TODO Later
            $userIp = $this->getRequest()->getClientIp();
        }


        //Refreshing the online experience
        //$this->getUserService()->refreshUserOnline();


        //Try to get the feature
        /**
         * @var Feature $feature
         */
        $feature = $this->getFeatureService()->getFeatureByNameAndGrouping($featureName,$featureGrouping);
        if ($feature !== null) {
            if ($feature->getEnable()) {
                if ($toReturn['data']['user']->getId()) {

                    //Check if user has access to this feature.
                    if ($toReturn['data']['user']->getRole()) {
                        $featureIds = $this->getRoleService()->getFeatureIdsByRole($toReturn['data']['user']->getRole());
                        if (in_array($feature->getId(),$featureIds)) {
                            // USER HAS ACCESS
                            $toReturn['access']['right'] = true;
                            if (!is_null($feature->getTitle())) {
                                $toReturn['data']['title'] = $feature->getTitle();
                            }

                            $toReturn['data']['access']['list'] = $this->getRoleService()->getFeaturesByRole($toReturn['data']['user']->getRole());

                        }else{
                            // USER HAS NO ACCESS
                            $toReturn['access']['message'] = "Sorry, You don't have access to this feature <br/> Please Contact Site Admin.";
                        }
                    }else{
                        $toReturn['access']['message'] = "Sorry, A problem has occurred. We will soon fix it.";
                    }

                }else{
                    // no user
                    if ($this->getFeatureService()->isFeaturePublic($feature)) {
                        $toReturn['access']['right'] = true;
                    }else{
                        $toReturn['access']['message'] = "Sorry, You need to log in to continue. ";
                        $toReturn['access']['method'] = ACCESS_METHOD::LOGIN;
                        $toReturn['access']['url'] = $this->getRequest()->getRequestUri();
                    }
                }
            }else{
                $toReturn['access']['message'] = "Sorry, The Feature You Are Trying To View Is Currently Disabled.";
            }
        }else{
            $toReturn['access']['message'] = "You are trying to access an invalid feature.";
        }


        return $toReturn;

    }

    /**
     * @return UserService
     */
    public function getUserService()
    {
        return $this->userService;
    }

    /**
     * @param UserService $userService
     */
    public function setUserService($userService)
    {
        $this->userService = $userService;
    }


    public function getFeatureService()
    {
        return $this->getContainer()->get("rush_framework_core.service.feature");
    }

    public function getRoleService()
    {
        return $this->getContainer()->get("rush_framework_core.service.role");
    }

}