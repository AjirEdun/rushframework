<?php

namespace RushFramework\CoreBundle\Service;

use RushFramework\CoreBundle\Entity\Feature;
use RushFramework\CoreBundle\Entity\Role;
use Doctrine\ORM\EntityManager;

class RoleService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FeatureService
     */
    protected $featureService;

    /**
     * RoleService constructor.
     * @param EntityManager $entityManager
     * @param FeatureService $featureService
     */
    public function __construct(EntityManager $entityManager, FeatureService $featureService)
    {
        $this->em = $entityManager;
        $this->featureService = $featureService;
    }

    /**
     * @param Role $role
     * @param Role|null $toCopy
     * @return bool
     */
    public function addRole(Role $role, Role $toCopy = null)
    {
        // THERE ARE TWO THINGS : 1) Add role 2)Add in featureaccess table

        // FORMAT ROLE NAME
        $role->setName($this->format($role->getName()));
        if ($roleExist = $this->getRoleByName($role->getName())) {
            return false;
        }else{
            $this->getEntityManager()->persist($role);
            $this->getEntityManager()->flush();
        }
        if ($toCopy) {
            $features = $this->getFeatureIdsByRole($toCopy);
            $formatKey = "";
            $formatValue = "";
            foreach ($features as $featureId){
                $formatKey .= "`f".$featureId."` ,";
                $formatValue .= "1 ,";
            }
            if ($formatKey !== "") {
                $formatKey = str_split($formatKey, strlen($formatKey)-1)[0];
                $formatValue = str_split($formatValue, strlen($formatValue)-1)[0];
            }
            $connection = $this->getEntityManager()->getConnection();
            $sql= "INSERT INTO `featureaccess` (`role_id`,".$formatKey.") VALUES (".$role->getId().",".$formatValue.") ; ";
            $connection->exec($sql);
        }else{
            $connection = $this->getEntityManager()->getConnection();
            $sql= "INSERT INTO `featureaccess` (`role_id`) VALUES (".$role->getId().") ; ";
            $connection->exec($sql);
        }
        return true;
    }

    /**
     * @param Role $role
     * @return array
     */
    public function getFeaturesByRole(Role $role)
    {
        $features = $this->getFeatureIdsByRole($role);
        if (count($features) > 0) {
            $features = $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Feature")->getFeaturesByIdsQueryBuilder($features);

        }
        return $features;
    }

    /**
     * @param Role $role
     * @return array
     */
    public function getFeaturesByRoleGroupByGrouping(Role $role)
    {
        $features = $this->getFeatureService()->getAllFeatures();
        $grouped = array();

        $featureIds = $this->getFeatureIdsByRole($role);

        if (count($features)>0) {
            foreach ($features as $feature) {
                /**
                 * @var Feature $feature
                 */
                if (array_key_exists($feature->getGrouping(),$grouped)) {
                    $array = array(
                        'feature' => $feature,
                        'access' => in_array($feature->getId(),$featureIds)? true : false
                    );
                    $grouped[$feature->getGrouping()][] = $array;
                }else{
                    $grouped[$feature->getGrouping()] = array();
                    $array = array(
                        'feature' => $feature,
                        'access' => in_array($feature->getId(),$featureIds)? true : false
                    );
                    $grouped[$feature->getGrouping()][] = $array;
                }
            }
        }


        return $grouped;
    }

    /**
     * @param Role $role
     * @return array
     */
    public function getFeatureIdsByRole(Role $role)
    {
        $features = array();

        $connection = $this->getEntityManager()->getConnection();
        $sql= "SELECT * FROM `featureaccess` WHERE role_id = ". $role->getId() ." ; ";
        $result = $connection->fetchAll($sql);

        if (count($result) > 0) {
            foreach ($result[0] as $key => $value) {
                if (($key !== "id") && ($key !== "role_id")) {
                    $featureId = str_replace("f","",$key);
                    if (intval($value) == 1) {
                        $features[] = $featureId;
                    }
                }
            }
        }

        return $features;
    }

    /**
     * @param Role $role
     * @return bool
     */
    public function removeRole(Role $role){
        if (($role->getName() !== \RushFramework\CoreBundle\Constants\ROLE::ADMIN_ROLE) &&
            ($role->getName() !== \RushFramework\CoreBundle\Constants\ROLE::DEFAULT_ROLE)){
            $this->getEntityManager()->remove($role);
            $this->getEntityManager()->flush();
        }
        return false;
    }

    /**
     * @param $roleName
     * @return mixed
     */
    public function getRoleByName($roleName)
    {
        $roleName = $this->format($roleName);
        return $this->getEntityManager()->getRepository("RushFrameworkCoreBundle:Role")->findOneByName($roleName);
    }

    /**
     * @param $toFormat
     * @return string
     */
    public function format($toFormat)
    {
        return strtoupper(trim($toFormat));
    }

    /**
     * @return FeatureService
     */
    public function getFeatureService()
    {
        return $this->featureService;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->em;
    }
}