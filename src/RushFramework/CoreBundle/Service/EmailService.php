<?php

namespace RushFramework\CoreBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class EmailService
{
    const NAME = 'rush_framework_core.service.email';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $admins;


    public function __construct(ContainerInterface $container, array $admins)
    {
        $this->container = $container;
        $this->admins = $admins;
    }

    public function sendMail($to, $subject, $body, array $params = array(), array $options = array())
    {
        $from = $this->container->getParameter('mailer_user');
        $this->sendMailFrom($to, $from, $subject, $body, $params, $options);
    }

    public function sendSnippetMailFrom($to, $from, $subject, $snippet,  $title = null, $credits = null, array $options = array())
    {
        $twig = $this->container->get('templating');

        $email = \Swift_Message::newInstance()
            ->setSubject($subject);
        if (isset($options['from_alias'])) {
            $email->setFrom($from, $options['from_alias']);
        }else {
            $email->setFrom($from);
        }

        $email->setTo($to)
            ->setBody(
                $twig->render(
                    "@RushFrameworkFrontOffice/Email/generic.html.twig",
                    array('header'=>$title, 'body'=>$snippet, 'footer' => $credits)
                ), 'text/html');

        $this->container->get('mailer')->send($email);
    }

    public function sendSnippetMailFromAdmin($to, $subject, $snippet,  $title = null, $credits = null, array $options = array())
    {
        $from = $this->container->getParameter('mailer_user');

        $this->sendSnippetMailFrom($to, $from, $subject, $snippet,  $title, $credits, $options);
    }

    public function sendSnippetMailToAdmin($subject, $snippet,  $title = null, $credits = null, array $options = array())
    {
        $from = $this->container->getParameter('mailer_user');

        if (empty($this->admins)) {
            throw new \Exception("You should define the rush_framework_core.admins parameters");
        }

        foreach ($this->admins as $admin) {
            $this->sendSnippetMailFrom($admin['email'], $from, $subject, $snippet,  $title, $credits, $options);
        }


    }


    public function sendMailFrom($to, $from, $subject, $body, array $params = array(), array $options = array())
    {
        $twig = $this->container->get('templating');

        $email = \Swift_Message::newInstance()
            ->setSubject($subject);
        if (isset($options['from_alias'])) {
            $email->setFrom($from, $options['from_alias']);
        }else {
            $email->setFrom($from);
        }

        $email->setTo($to)
            ->setBody(
                $twig->render(
                    $body,
                    $params
                ), 'text/html');

        $this->container->get('mailer')->send($email);
    }

    public function sendMailToAdmin($subject, $body, array $params = array(), array $options = array())
    {
        $from = $this->container->getParameter('mailer_user');

        if (empty($this->admins)) {
            throw new \Exception("You should define the rush_framework_core.admins parameters");
        }

        foreach ($this->admins as $admin) {
            $this->sendMailFrom($admin['email'], $from, $subject, $body, $params, $options);
        }
    }
}