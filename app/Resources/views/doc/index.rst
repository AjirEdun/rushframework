Rush Framework
==============

Rush Framework is a CMF built to accelerate the development of web applications.
It contains the basic mechanisms and architectures that modern web applications use.


Rush Framework Bundles?
-----------------------

Rush Framework is very extensible as it consists of many bundles, each of which has their own specific purpose.


List of Bundles
---------------

* CoreBundle
* RenderBundle
* BackOfficeBundle